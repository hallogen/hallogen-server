var noId = "00000000000000000000000000000000";

function makeSpecialCellId(specialId: number) {
    // Pad to two digits. Oh javascript...
    return noId + String("00" + specialId).slice(-2);
}

var constants = {
    apiVersion: "v1",
    resourcePaths: {
        cell: "/cells",
        auth: "/auth"
    },
    specialIds: {
        noId: noId,
        
        rootCellId: makeSpecialCellId(1),
        participantsHomeCellId: makeSpecialCellId(2),
        
        originParticipantId: makeSpecialCellId(50)
    },
    neo4j: {
        cells: {
            CellNodeLabel: "Cell",
            HasChildRelation: "HAS_CHILD",
            CreatedByRelation: "CREATED_BY"
        },
        participants: {
            ParticipantNodeLabel: "Participant"
        },
        content: {
            ContentNodeLabel: "Content",
            OwnedByRelation: "OWNED_BY"
        }
    }
};

Object.freeze(constants);
Object.freeze(constants.resourcePaths);
Object.freeze(constants.specialIds);
Object.freeze(constants.neo4j.cells);
Object.freeze(constants.neo4j.participants);
Object.freeze(constants.neo4j);

export = constants;