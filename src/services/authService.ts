import restify = require("restify");
import passport = require("passport");
import passportLocal = require("passport-local");
import participantService = require("../data/participantDataService");
import halCrypto = require("../utils/halCrypto");
import config = require("../config");
import commonRes = require("../common/types/resource");
import e = require("../api/types/errors");
var log = require("../utils/log").getLog(module);
var jwt = require("jsonwebtoken");
var BearerStrategy = <any>require("passport-http-bearer").Strategy;

export function initialize() : restify.RequestHandler {
    // Passport setup
    passport.use(new HallogenAuthStrategy());
    passport.use(createBearerStrategy());
    
    // Wrap the passport initialization.
    return <any>passport.initialize();
}

export function createToken(data) {
    var token = jwt.sign(data, config.tokenSecret);
    return token;
}

function createBearerStrategy() {
    return new BearerStrategy({
        },
        function(token, done) {
            process.nextTick(function () {
                // Find the user by token. If there is no user with the given token, set
                // the user to `false` to indicate failure. Otherwise, return the
                // authenticated `user`.
                jwt.verify(token, config.tokenSecret, function(err, decoded) {
                    if (err) {
                        log.error(err);
                    }
                    if (err || !decoded) {
                        done(null, false);
                    }
                    else {
                        done(null, decoded.participant);
                    }
                });
            });
        });
}

class HallogenAuthStrategy extends passportLocal.Strategy {
    constructor() {
        super(this.verify);
    }
    private verify(username: string,
                   password: string,
                   done: (error: any, user?: any, options?: passportLocal.IVerifyOptions) => void) {
        participantService.findUserByCredentials(username, halCrypto.sha256(password))
            .then(function(user) {
                done(null, user);
            }, function(err) {
                done(null, false);
            });
    }
}

export function authBearer(): any {
    return [ 
        passport.authenticate("bearer", { session: false }),
        function(req, res, next: restify.Next) {
            if (req.user && req) {
                req.participant = req.user;
                return next();
            }
            else {
                return next(e.createFromMsg(e.InternalError, "Missing a token bearer."));
            }
        }
    ];
}

var passportLocalAuth = passport.authenticate("local", { session: false });
function performCredentialAuth(req, res, next) {
    if (req.body.username && req.body.password) {
        return passportLocalAuth(req, res, next);
    }
    else {
        return next(e.createFromMsg(e.UnauthorizedError, "Unknown or no credentials provided."));
    }
}

export var createAccessTokenRequest = <Array<restify.RequestHandler>>[
    performCredentialAuth,
    function(req, res, next) {
        var token = createToken({ participant: (<any>req).user });
        res.send({
            bearerToken: token
        });
        return next();
    }
];