import Promise = require("bluebird");
import cellDataSvc = require("../data/cellDataService");
import errors = require("../api/types/errors");
import constants = require("../constants");
import t = require("../types");
import _ = require("lodash");

var AccessTypes = t.common.permission.AccessTypes;

function satisfiesAccessType(minAccessRight: string, right: string) {
    if (minAccessRight === AccessTypes.read) {
        return right === AccessTypes.read || 
               right === AccessTypes.append; 
    }
    else if (minAccessRight === AccessTypes.append) {
        return right === AccessTypes.append;
    }
    else {
        throw new Error("Unknown AccessType: " + minAccessRight);
    }
}

function containsMatchingPermission(permissionList: t.api.permission.Permission[], permission: t.api.permission.Permission) {
    return _.any(permissionList, p => {
        // The access type and permission recipient must match. The public recipient (noId) matches all users. 
        return satisfiesAccessType(permission.accessType, p.accessType) &&
               (p.recipient.id === permission.recipient.id || p.recipient.id === constants.specialIds.noId);
    });
}

export function checkPermission(cellRef: t.common.resource.ResourceReference,
                                permission: t.api.permission.Permission): Promise<boolean|Promise<boolean>> { // Should always return a Promise<boolean> but the compiler doesn't get it.
    
    return cellDataSvc.findCreatorAsync(cellRef)
        .then(creator => {
            if (!creator) {
                throw errors.createFromMsg(errors.BadRequestError, "The creator of the cell does not exist.");
            }
            else if (creator.id === permission.recipient.id) {
                // Creators are always allowed to read/append to their cells.
                return true;
            }
            else {
                return cellDataSvc.getCellPermissionsAsync(cellRef)
                    .then(permissions => {
                        if (!permissions) {
                            throw errors.createFromMsg(errors.InternalError, "Parent cell does not have valid permissions.");
                        }
                        else if (permissions.length === 0) {
                            return false;
                        }
                        else {
                            return containsMatchingPermission(permissions, permission);
                        }
                    });
            }
        });
}