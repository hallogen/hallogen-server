import participantDataSvc = require("../data/participantDataService");
import cellDataSvc = require("../data/cellDataService");
import halCrypto = require("../utils/halCrypto");

export function createUserParticipant(username, password) {
    var pwhash = halCrypto.sha256(password);
    return participantDataSvc.addUserAsync(username, pwhash)
        .then(newParticipant => {
            return cellDataSvc.createHomeCell(newParticipant)
                .then(homeCell => newParticipant);
        });
}