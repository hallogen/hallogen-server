import JaySchema = require("jayschema");
import deref = require("json-schema-deref");
import Promise = require("bluebird");
import fs = require("fs");
import path = require("path");
import config = require("../config");
import utils = require("../utils/utils");
import logModule = require("../utils/log");
var log = logModule.getLog("jsonSchemaValidator");

var validator = new JaySchema();

function getSchemaForClassName(moduleName, className) {
    return moduleName + ".json#/definitions/" + className;
}

export var errors = JaySchema.errors;

var schemaFilesLoaded = false;
var schemaFilesLoader = null;
export function loadSchemaFiles(): Promise<any> {
    if (!schemaFilesLoaded && !schemaFilesLoader) {
        schemaFilesLoaded = true;
        schemaFilesLoader = new Promise(function(resolve: (r:any) => void, reject) {
            fs.readdir(config.schemaPath, function(err, files) {
                if (err) {
                    reject(err);
                }
                else if (!files.length) {
                    reject(new Error("No JSON schema files found."));
                }
                else {
                    var readFilesPromises = files.filter(f => utils.stringEndsWith(f, ".json"))
                        .map(f => 
                            new Promise<any>((resolve: (r:any) => void, reject) => {
                                fs.readFile(path.join(config.schemaPath, f), {encoding: "UTF8"}, function (err, contents) {
                                    if (err) {
                                        reject(err);
                                    }
                                    else {
                                        try {
                                            var schemaObj = JSON.parse(contents);
                                            resolve({
                                                schema: schemaObj,
                                                file: f
                                            });
                                        }
                                        catch (e) {
                                            reject(e);
                                        }
                                    }
                                });
                            })
                        );
                    
                    var schemaDerefPromises = Promise.all(readFilesPromises)
                        .then((loadedSchemas: Array<any>) => {
                            return loadedSchemas.map(r => 
                                new Promise((resolve, reject) => {
                                    deref(r.schema, {
                                        baseFolder: config.schemaPath
                                    }, (err, fullSchema) => {
                                        if (err) {
                                            reject(err);
                                        }
                                        else {
                                            r.schema = fullSchema;
                                            resolve(r);
                                        }
                                    });  
                                })
                            );
                        });
                    
                    Promise.all(schemaDerefPromises).then((loadedSchemas: Array<any>) => {
                        try {
                            loadedSchemas.forEach(s => {
                                validator.register(s.schema, path.basename(s.file));
                            });
                            resolve(loadedSchemas);
                        }
                        catch(e) {
                            reject(e);
                        }
                    }, e => reject(e));
                }
            });
        })
        .catch(e => {
            log.error("Could not read json schema files", e);
            throw e;
        })
        .finally(() => {
            schemaFilesLoader = null;        
        });
    }
    return schemaFilesLoader;
}

export function validate<T>(obj, moduleName, className): Promise<boolean> {
    var schemaFileLoader = loadSchemaFiles();
    if (schemaFileLoader) {
        return schemaFileLoader.then(validate);
    }
    else {
        return validate();
    }
    
    function validate() {
        return new Promise<boolean>(function (resolve:(result:boolean) => void, reject) {
            validator.validate(obj, getSchemaForClassName(moduleName, className), function (err) {
                if (err) {
                    // We should send all messages, but need a better way to format them
                    // or support this on errors messages sent to the client.
                    if (Array.isArray(err)) {
                        err = err[0];
                    }
                    reject(err);
                }
                else {
                    resolve(true);
                }
            });
        });
    }
}

