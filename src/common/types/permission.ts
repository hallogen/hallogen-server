var _AccessTypes = {
    read: "read",
    append: "append"
};
Object.freeze(_AccessTypes);
export var AccessTypes = _AccessTypes;