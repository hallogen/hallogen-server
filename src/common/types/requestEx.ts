/// <reference path="../../../typings/tsd.d.ts" />
import restify = require("restify");
import dParticipant = require("../../data/types/participant");

interface HallogenRequest extends restify.Request {
    participant: dParticipant.ParticipantData;
}
export = HallogenRequest;