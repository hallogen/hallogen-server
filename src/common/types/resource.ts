//export interface ResourceId extends String {}
export type ResourceId = string;

export interface ResourceReference {
    id: ResourceId;
    href?: string;
}

export class IdResourceReference implements ResourceReference {
    constructor(public id: ResourceId) {
    }
}

export class HResourceReference implements ResourceReference {
    constructor(public id: ResourceId, public href: string) {
        
    }
}