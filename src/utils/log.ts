import bunyan = require("bunyan");
import path = require("path");
import config = require("../config");
var bformat: any = require("bunyan-format");
var formatOut = bformat({ outputMode: "short" });

export const enum LogLevel {
    trace,
    debug,
    info,
    warn,
    error,
    fatal
}
var logLevelStrings = {};
logLevelStrings[LogLevel.trace] = "trace";
logLevelStrings[LogLevel.debug] = "debug";
logLevelStrings[LogLevel.info] = "info";
logLevelStrings[LogLevel.warn] = "warn";
logLevelStrings[LogLevel.error] = "error";
logLevelStrings[LogLevel.fatal] = "fatal";

var defaultErrorSerializer = (<any>bunyan.stdSerializers).err;
function errorSerializer(err) {
    if (err.instanceContext && !err.message) {
        // JSON validation error
        err.message = JSON.stringify(err);
    }
    
    var defaultError = defaultErrorSerializer(err);
    if (err.neo4jError) {
        defaultError.message = defaultError.message + "\nNeo4jError:\n" + 
                               JSON.stringify(err.neo4jError, null, 4);
    }
    return defaultError;
}

var rootLogger: bunyan.Logger;
function getRootLogger() {
    if (!rootLogger) {
        rootLogger = bunyan.createLogger({
            name: config.serverName,
            stream: formatOut, //process.stdout,
            level: logLevelStrings[LogLevel.debug],
            serializers: {
                err: errorSerializer
            }
        });
    }
    return rootLogger;
}

export function getLog(name: String|Object) {
    if (name instanceof Object && name["id"]) {
        name = path.basename(name["id"]);
    }
    
    return getRootLogger().child({
        loggerId: name
    });
}