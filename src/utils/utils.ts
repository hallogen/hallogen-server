import uuid = require("node-uuid");
import t = require("../types");
import constants = require("../constants");
import config = require("../config");

export function newId() //: resource.ResourceId
{
    return uuid.v1().replace(/-/g, "");
}

export function applyBasePath(path) {
    return "/" + constants.apiVersion + path;
}

export function makeRelativePath(absolutePath: string): string {
    return absolutePath.replace(config.baseUri, "/");
}

export function getApiBasePath(): string {
    return config.baseUri + constants.apiVersion;
}

export function stringEndsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
 
var idRegex = /^[a-zA-Z0-9]{32,34}$/;
var idExtractRegex = /\/([a-zA-Z0-9]{32,34})$/;
var _refs = {
    isValidId: function(id: t.common.resource.ResourceId) {
        return idRegex.test(id);
    },
    extractId: function(href: t.common.resource.ResourceId) {
        var match = idExtractRegex.exec(href);
        if (match && match.length) {
            return match[1];
        }
        else {
            return null;
        }
    },
    makeFullCellRef: function (ref: t.common.resource.ResourceReference): t.common.resource.ResourceReference {
        return new t.common.resource.HResourceReference(
            ref.id,
            getApiBasePath() + constants.resourcePaths.cell + "/" + ref.id
        );
    },
    makeFullContentRef: function (content: t.data.content.ContentData): t.common.resource.ResourceReference {
        return new t.common.resource.HResourceReference(
            content.ref.id,
            getApiBasePath() + constants.resourcePaths.cell + "/" + content.owner.id + "/content"  
        );
    }
};
export var refs = _refs;