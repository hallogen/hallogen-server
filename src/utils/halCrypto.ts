/// <reference path="../../typings/tsd.d.ts" />
import crypto = require("crypto");

export function sha256(input: string) : string {
    var hash = crypto.createHash("sha256").update(input).digest("base64");
    return hash;
}