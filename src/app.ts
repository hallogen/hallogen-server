﻿import server = require("./server");
import config = require("./config");
import jsonSchemaValidator = require("./services/jsonSchemaValidator");
import logModule = require("./utils/log");
var log = logModule.getLog("app");

jsonSchemaValidator.loadSchemaFiles().then(() => {
    log.info("JSON schema files preloaded successfully.");
    server.listen(config.port, function () {
        console.log("%s listening at %s", server.name, server.url);
    });
}, e => {
    log.error("Could not load JSON schema files.", e);
});

