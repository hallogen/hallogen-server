/// <reference path="../typings/tsd.d.ts" />
import fs = require("fs");
import os = require("os");
import path = require("path");
import _ = require("lodash");

var env: string = process.env.NODE_ENV || "dev";
var hostname: string = os.hostname();
var configPath = "config";

function createConfigFilePath(baseConfigName: string, env?: string) {
    var filename;
    if (env) {
        filename = baseConfigName + "_" + env + ".json";
    }
    else {
        filename = baseConfigName + ".json";
    }
    return path.join(configPath, filename);
}

// The config files are read in this order, with the last being valued highest.
var configFiles = [
    createConfigFilePath("default"),
    createConfigFilePath("default", env),
    createConfigFilePath(hostname),
    createConfigFilePath(hostname, env),
];

/**
 * Reads a config file.
 * @param filename The file to read.
 * @returns {Config} The config or null if the file does not exist or something goes wrong.
 */
function readConfig(filename) : Config {
    try {
        if (fs.existsSync(filename)) {
            var str = fs.readFileSync(filename).toString("utf8");
            return <Config>JSON.parse(str);
        }
        else {
            return null;
        }
    }
    catch(e) {
        return null;
    }
}

var readConfigFiles = configFiles.map(f => readConfig(f));
if (_.every(readConfigFiles,  f => f == null)) {
    throw new Error("No config file found.");
}
var config = _.assign.apply(_, [{}].concat(readConfigFiles));

/**
 * The application's global configuration.
 */
interface Config {
    baseUri: string;
    serverName: string;
    /**
     * The server's listening port.
     */
    port: string;
    /**
     * The configuration object for neo4j.
     */
    neo4jdb: string;
    /**
     * The directory for the level db storage.
     * */
    leveldbDir: string;
    /**
     * The secret to use for bearer token encryption.
     */
    tokenSecret: string;
    debug: boolean;
    schemaPath: string;
}

export = config;