import restify = require("restify");
import passport = require("passport");
import config = require("./config");
import routes = require("./routes");
import log = require("./utils/log");
import authSvc = require("./services/authService");
import t = require("./types");

var rootLog = log.getLog("");
// Server setup
var server = restify.createServer({
    name: config.serverName,
    version: "1.0.0",
    log: rootLog
});
server.use(restify.bodyParser());
server.use(restify.queryParser());
//server.use(sessions({
//    cookieName: "session", // cookie name dictates the key name added to the request object
//    secret: config.sessionSecret, // should be a large unguessable string
//    duration: 24 * 60 * 60 * 1000, // 24 hours
//    activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
//}));
//server.use(<any>passport.initialize());
//server.use(<any>passport.session());
server.use(authSvc.initialize());
server.use(restify.CORS());

//server.on("after", function(req, res, route, error) {
//    if (config.debug) {
//        rootLog.info("Request for " + req.path());
//    }
//});

server.on("uncaughtException", function (request, response, route, error) {
    rootLog.error(error);
    //process.exit(1);
    if (config.debug) {
        response.send(error);
    }
    else {
        response.send(t.api.errors.createFromMsg(t.api.errors.InternalServerError, ""));
    }
});

// Routes are set in a separate module.
routes.setup(server);

export = server;
