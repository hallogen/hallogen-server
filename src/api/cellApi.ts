import restify = require("restify");
import Promise = require("bluebird");
import t = require("../types");
import contentFac = require("../data/contentFactory");
import contentDataSvc = require("../data/contentDataService");
import cellFac = require("../data/cellFactory");
import cellDataSvc = require("../data/cellDataService");
import cellSvc = require("../services/cellService");
import authSvc = require("../services/authService");
import logModule = require("../utils/log");
import utils = require("../utils/utils");
import errors = require("./types/errors");
import jsonValidator = require("../services/jsonSchemaValidator");
var log = logModule.getLog("cellApi");

type CellIn = t.api.cell.CellIn;
type CellOut = t.api.cell.CellOut;
type CellData = t.data.cell.CellData;
type ParticipantData = t.data.participant.ParticipantData;

export var createCellRequest = [
    authSvc.authBearer(),
    cellPost
];

export var getCellRequest = [
    authSvc.authBearer(),
    cellGet
];

function cellGet(req: t.common.RequestEx, res: restify.Response, next: restify.Next) {
    var cellId = req.params.cellid;
    if (!utils.refs.isValidId(cellId)) {
        return next(t.api.errors.createFromMsg(t.api.errors.BadRequestError, "Invalid cell id."));
    }
    else {
        cellDataSvc.getCellAsync(new t.common.resource.IdResourceReference(cellId))
            .then(cellData => {
                if (!cellData) {
                    throw t.api.errors.createFromMsg(t.api.errors.ResourceNotFoundError, "Cell not found.");
                }
                else {
                    return cellSvc.checkPermission(cellData.ref, new t.data.permission.PermissionData(req.participant.ref, t.common.permission.AccessTypes.read))
                        .then(function(hasPermission) {
                            if (hasPermission) {
                                var cellOut = cellFac.makeCellOut(cellData);
                                res.send(200, cellOut);
                                next();   
                            }
                            else {
                                throw t.api.errors.createFromMsg(t.api.errors.ForbiddenError, "You are not allowed to read the cell with id " + cellData.ref.id + ".");
                            }
                        });
                }
            })
            .catch(err => {
                log.error(err);
                return next(err);
            });
        
    }
}

function cellPost(req: t.common.RequestEx, res: restify.Response, next: restify.Next) {
    
    validateCellIn(req.body)
        .then(validatedCell => createCellAsync(validatedCell, req.participant))
        .then(function(cellOut) {
            res.send(201, cellOut);
            return next();
        }, function(err) {
            if (err instanceof jsonValidator.errors.ValidationError) {
                var e = t.api.errors.createFromMsg(t.api.errors.BadRequestError, JSON.stringify(err));
                return next(e);
            }
            log.error(err);
            return next(err);
        });
}

function validateCellIn(cellIn: CellIn) {
    return jsonValidator.validate(cellIn, "cell", "CellIn")
        .then(() => {
            // TODO: validate things like content size?
            return cellIn;
        });
}

function createCellAsync(cellIn: CellIn, participant: ParticipantData) : Promise<CellOut> {
    // Create local cell object from input.
    var cellData = cellFac.prepareNewCell(cellIn, participant);
    
    // Check if the participant is allowed to append to the parent.
    return cellSvc.checkPermission(cellData.parent, { 
            recipient: participant.ref,
            accessType: t.common.permission.AccessTypes.append
        })
        .then(hasPermission => {
            if (!hasPermission) {
                throw errors.createFromMsg(errors.ForbiddenError, "You don't have the permission to append to cell " + cellData.parent.id + ".");
            }
        })
        .then(() => {
            var storeContentFn;
            
            var contentIn = <any>cellIn.content;
            if (contentIn.id) {
                // The cell references an existing content.
                cellData.content = <t.common.resource.ResourceReference>cellIn.content;
                storeContentFn = () => {};
            }
            else {
                // The cell references new content that we have to add to the database.
                var newContent = contentFac.prepareNewContent(contentIn, cellData.ref);
                cellData.content = newContent.ref;
                // This function must be called after the cell was created.
                storeContentFn = () => contentDataSvc.addContentAsync(newContent);
            }

            // Store the cell.
            return cellDataSvc.addCellAsync(cellData, false)
                // We might have to store new content which is done asynchronously.
                .then(storeContentFn)
                // Retrieve the content to append it to the result.
                .then(() => contentDataSvc.getContentAsync(cellData.content))
                .catch(e => {
                    // Remove the created cell again in case of an error.
                    return cellDataSvc.deleteCellAsync(cellData)
                        .finally(() => Promise.reject(e));
                })
                .then(content => {
                    // Prepare the cell object for sending to the client.
                    var cellOut = cellFac.makeCellOut(cellData);
                    cellOut.content = contentFac.makeContentOut(content);
                    return cellOut;
                });
        });
}
