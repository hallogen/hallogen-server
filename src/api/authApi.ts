import restify = require("restify");
import Promise = require("bluebird");
import config = require("./../config");
import participant = require("./types/participant");
import participantDataSvc = require("../data/participantDataService");
import participantFac = require("../data/participantFactory");
import participantSvc = require("../services/participantService");
import halCrypto = require("../utils/halCrypto");
import authSvc = require("../services/authService");
import e = require("./types/errors");

export var createAccessToken = authSvc.createAccessTokenRequest;

export function createParticipant(req, res, next) {
    if (req.body.username && req.body.password) {
        return createUserParticipant(req.body, res, next);
    }
    else {
        return next(e.createFromMsg(e.BadRequestError, "Unknown participant type requested."))
    }
}

function createUserParticipant(userIn: participant.UserParticipantIn, res, next) {
    var usernameRegex = /^[a-zA-Z0-9\-_]{1,100}$/;
    var pwRegex = /^.{5,}$/;

    var username = userIn.username;
    if (!usernameRegex.test(username)) {
        return next(new restify.BadRequestError("Invalid username. Usernames must only contain lower/upper case letters and numbers and have max 100 chars."));
    }

    var password = userIn.password;
    if (!pwRegex.test(password)) {
        return next(new restify.BadRequestError("Invalid password. Passwords must have at least 5 chars."));
    }

    participantDataSvc.findUserByNameAsync(username)
        .then(
            existingUser => { throw new restify.BadRequestError("User already exists."); }, 
            () => { /* Handle error */ })
        .then(() => participantSvc.createUserParticipant(username, password))
        .then(newParticipant => {
            var participantOut = participantFac.makeParticipantOut(newParticipant);
            return participantOut;
        })
        .then(partOut => res.send(201, partOut))
        .catch(err => next(err));
}

export var whoami = [
    authSvc.authBearer(),
    function(req, res, next) {
        if (!req.isAuthenticated()) {
            next(new restify.NotAuthorizedError("Not logged in."));
        }
        else {
            res.send(req.user);
            next();
        }
    }
];
