/// <reference path="../../../typings/tsd.d.ts" />
import restify = require("restify");

export var UnspecifiedErrorCode = 1000;

export interface HallogenErrorBody {
    code: number;
    message: string;
    userMessage?: string;
    moreInfoLink?: string;
}

export interface HallogenErrorOptions {
    message: string;
    userMessage?: string;
    moreInfoLink?: string;
    errorCode: number;
    statusCode?: number;
    cause?: Error;
}

interface HallogenErrorSubclassOptions {
    name: string;
    ctor: Function; 
    statusCode: number;
}

export class HallogenError extends restify.RestError {
    code: number;
    message: string;
    name: string;
    statusCode: number;
    body: HallogenErrorBody;
    
    /**
     * Creates a new HallogenErrorBase.
     * @param options Either a set of options, a cause for the error, a message or an HTTP status code.
     */
    constructor(options: HallogenErrorOptions, 
                subclassingOptions?: HallogenErrorSubclassOptions) {
       
        var name = "HallogenError";
        var ctor: Function = HallogenError;
        
        if (subclassingOptions) {
            options.statusCode = subclassingOptions.statusCode;
            name = subclassingOptions.name;
            ctor = subclassingOptions.ctor;
        }
        
        super({
            restCode: options.errorCode || options.statusCode,
            statusCode: options.statusCode || 500, 
            message: options.message,
            constructorOpt: ctor,
            cause: Error
        });
        this.name = name;
        
        if (options.userMessage) {
            this.body.userMessage = options.userMessage;
        }
        if (options.moreInfoLink) {
            this.body.moreInfoLink = options.moreInfoLink;
        }
    }
}

interface HallogenErrorConstructor<T> {
    new(options:HallogenErrorOptions): T
}

export function createFromError<T extends HallogenError>(errorType: HallogenErrorConstructor<T>, e: Error): T {
    return create<T>(errorType, e.toString(), undefined, undefined, UnspecifiedErrorCode);
}

export function createFromMsg<T extends HallogenError>(errorType: HallogenErrorConstructor<T>, message: string): T {
    return create<T>(errorType, message, undefined, undefined, UnspecifiedErrorCode);
}

export function createFromMsgErrorCode<T extends HallogenError>(errorType: HallogenErrorConstructor<T>, message: string, errorCode: number): T {
    return create<T>(errorType, message, undefined, undefined, errorCode);
}

export function createFromMsgErrorCodeUserMsg<T extends HallogenError>(errorType: HallogenErrorConstructor<T>, 
                                                                       message: string, userMsg: string, errorCode: number): T {
    return create<T>(errorType, message, userMsg, undefined, errorCode);
}

export function create<T extends HallogenError>(errorType: HallogenErrorConstructor<T>,
                                                message: string, userMsg: string, moreInfoLink: string, errorCode: number): T {
    return new errorType({
        message: message,
        userMessage: userMsg,
        moreInfoLink: moreInfoLink,
        errorCode: errorCode
    })
}

// The following error classes are the same as in restify, but extending our own error class with additional properties.

export class BadRequestError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "BadRequestError", ctor: BadRequestError, statusCode: 400 });
    }
}
export class UnauthorizedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "UnauthorizedError", ctor: UnauthorizedError, statusCode: 401 });
    }
}
export class PaymentRequiredError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "PaymentRequiredError", ctor: PaymentRequiredError, statusCode: 402 });
    }
}
export class ForbiddenError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "ForbiddenError", ctor: ForbiddenError, statusCode: 403 });
    }
}
export class NotFoundError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "NotFoundError", ctor: NotFoundError, statusCode:
            404 });
    }
}
export class MethodNotAllowedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "MethodNotAllowedError", ctor: MethodNotAllowedError, statusCode: 405 });
    }
}
export class NotAcceptableError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "NotAcceptableError", ctor: NotAcceptableError, statusCode: 406 });
    }
}
export class ProxyAuthenticationRequiredError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "ProxyAuthenticationRequiredError", ctor: ProxyAuthenticationRequiredError, statusCode: 407 });
    }
}
export class RequestTimeoutError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "RequestTimeoutError", ctor: RequestTimeoutError,
            statusCode: 408 });
    }
}
export class ConflictError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "ConflictError", ctor: ConflictError, statusCode:
            409 });
    }
}
export class GoneError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "GoneError", ctor: GoneError, statusCode: 410 });

    }
}
export class LengthRequiredError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "LengthRequiredError", ctor: LengthRequiredError,
            statusCode: 411 });
    }
}
export class PreconditionFailedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "PreconditionFailedError", ctor: PreconditionFailedError, statusCode: 412 });
    }
}
export class RequestEntityTooLargeError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "RequestEntityTooLargeError", ctor: RequestEntityTooLargeError, statusCode: 413 });
    }
}
export class RequesturiTooLargeError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "RequesturiTooLargeError", ctor: RequesturiTooLargeError, statusCode: 414 });
    }
}
export class UnsupportedMediaTypeError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "UnsupportedMediaTypeError", ctor: UnsupportedMediaTypeError, statusCode: 415 });
    }
}
export class RequestedRangeNotSatisfiableError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "RequestedRangeNotSatisfiableError", ctor: RequestedRangeNotSatisfiableError, statusCode: 416 });
    }
}
export class ExpectationFailedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "ExpectationFailedError", ctor: ExpectationFailedError, statusCode: 417 });
    }
}
export class ImATeapotError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "ImATeapotError", ctor: ImATeapotError, statusCode: 418 });
    }
}
export class UnprocessableEntityError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "UnprocessableEntityError", ctor: UnprocessableEntityError, statusCode: 422 });
    }
}
export class LockedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "LockedError", ctor: LockedError, statusCode: 423
        });
    }
}
export class FailedDependencyError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "FailedDependencyError", ctor: FailedDependencyError, statusCode: 424 });
    }
}
export class UnorderedCollectionError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "UnorderedCollectionError", ctor: UnorderedCollectionError, statusCode: 425 });
    }
}
export class UpgradeRequiredError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "UpgradeRequiredError", ctor: UpgradeRequiredError, statusCode: 426 });
    }
}
export class PreconditionRequiredError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "PreconditionRequiredError", ctor: PreconditionRequiredError, statusCode: 428 });
    }
}
export class TooManyRequestsError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "TooManyRequestsError", ctor: TooManyRequestsError, statusCode: 429 });
    }
}
export class RequestHeaderFieldsTooLargeError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "RequestHeaderFieldsTooLargeError", ctor: RequestHeaderFieldsTooLargeError, statusCode: 431 });
    }
}
export class InternalServerError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "InternalServerError", ctor: InternalServerError,
            statusCode: 500 });
    }
}
export class NotImplementedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "NotImplementedError", ctor: NotImplementedError,
            statusCode: 501 });
    }
}
export class BadGatewayError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "BadGatewayError", ctor: BadGatewayError, statusCode: 502 });
    }
}
export class ServiceUnavailableError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "ServiceUnavailableError", ctor: ServiceUnavailableError, statusCode: 503 });
    }
}
export class GatewayTimeoutError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "GatewayTimeoutError", ctor: GatewayTimeoutError,
            statusCode: 504 });
    }
}
export class HttpVersionNotSupportedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "HttpVersionNotSupportedError", ctor: HttpVersionNotSupportedError, statusCode: 505 });
    }
}
export class VariantAlsoNegotiatesError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "VariantAlsoNegotiatesError", ctor: VariantAlsoNegotiatesError, statusCode: 506 });
    }
}
export class InsufficientStorageError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "InsufficientStorageError", ctor: InsufficientStorageError, statusCode: 507 });
    }
}
export class BandwidthLimitExceededError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "BandwidthLimitExceededError", ctor: BandwidthLimitExceededError, statusCode: 509 });
    }
}
export class NotExtendedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "NotExtendedError", ctor: NotExtendedError, statusCode: 510 });
    }
}
export class NetworkAuthenticationRequiredError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "NetworkAuthenticationRequiredError", ctor: NetworkAuthenticationRequiredError, statusCode: 511 });
    }
}
export class BadDigestError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "BadDigestError", ctor: BadDigestError, statusCode: 400 });
    }
}
export class BadMethodError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "BadMethodError", ctor: BadMethodError, statusCode: 405 });
    }
}
export class InternalError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "InternalError", ctor: InternalError, statusCode:
            500 });
    }
}
export class InvalidArgumentError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "InvalidArgumentError", ctor: InvalidArgumentError, statusCode: 409 });
    }
}
export class InvalidContentError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "InvalidContentError", ctor: InvalidContentError,
            statusCode: 400 });
    }
}
export class InvalidCredentialsError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "InvalidCredentialsError", ctor: InvalidCredentialsError, statusCode: 401 });
    }
}
export class InvalidHeaderError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "InvalidHeaderError", ctor: InvalidHeaderError, statusCode: 400 });
    }
}
export class InvalidVersionError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "InvalidVersionError", ctor: InvalidVersionError,
            statusCode: 400 });
    }
}
export class MissingParameterError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "MissingParameterError", ctor: MissingParameterError, statusCode: 409 });
    }
}
export class NotAuthorizedError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "NotAuthorizedError", ctor: NotAuthorizedError, statusCode: 403 });
    }
}
export class RequestExpiredError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "RequestExpiredError", ctor: RequestExpiredError,
            statusCode: 400 });
    }
}
export class RequestThrottledError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "RequestThrottledError", ctor: RequestThrottledError, statusCode: 429 });
    }
}
export class ResourceNotFoundError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "ResourceNotFoundError", ctor: ResourceNotFoundError, statusCode: 404 });
    }
}
export class WrongAcceptError extends HallogenError {
    constructor(options: HallogenErrorOptions) {
        super(options, { name: "WrongAcceptError", ctor: WrongAcceptError, statusCode: 406 });
    }
}
