import commonRes = require("../../common/types/resource");

export interface Permission {
    recipient: commonRes.ResourceReference;
    /**
     * @see {@link AccessTypes}
     */
    accessType: string;
}
