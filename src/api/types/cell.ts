import commonRes = require("../../common/types/resource");
import resource = require("resource");
import participant = require("participant");
import permission = require("permission");
import content = require("content");

export interface CellBase {
    parent: commonRes.ResourceReference;
    permissions: permission.Permission[];
}

export interface CellIn extends CellBase {
    content: content.ContentIn | commonRes.ResourceReference
}

export interface CellOut extends resource.Resource, CellBase {
    creator: participant.ParticipantOut;
    createdAt: Date;
    modifiedAt: Date;
    content: content.ContentOut;
    score: number;
}

export module actions {

    export var OrderDirections = Object.freeze({
        asc: "asc",
        desc: "desc"
    });
    
    export var DefaultOrderAttributes = Object.freeze({
        date: "date",
        score: "score"
    });
    
    export var DefaultScoreFunctions = Object.freeze({
        numChildren: "numChildren",
        newestChildren: "newestChildren"
    });
    
    export var TraversalDirections = Object.freeze({
        depth: "depth",
        breadth: "breadth"
    });

    export interface ExploreAction {
        /**
         * The maximum number walk steps to perform.
         * @default 25
         */
        limit?: number;
        /**
         * The order direction for the {@link ExploreAction#orderBy} attribute.
         * @see {@link OrderDirections}
         * @default "asc"
         */
        orderDir?: string;
        /**
         * The attributes by which to order by. 
         * Can contain multiple values to order by multiple criteria. 
         * This value only applies to the order of cells that have a common parent, not to all returned cells.
         * @see {@link DefaultOrderAttributes}
         * @default "date"
         */
        orderBy?: string;
        /**
         * The name of the function that determines a cell's score. 
         * A score function Cell -> int calculates a score value from a cell. 
         * The score can also be used as a sort criterion.
         * @see {@link DefaultScoreFunctions}
         * @default "numChildren"
         */
        scoreFn?: string;
        /**
         * The tree traversal direction.
         * @see {@link TraversalDirections}
         * @default "depth"
         */
        travDir?: string;
        /**
         * Limits the number of levels to traverse in the selected traversal direction (i.e. the max search depth or breadth).
         * @default -1 No limit.
         */
        travLimit?: number;
    }
}
