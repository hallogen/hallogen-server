import commonRes = require("../../common/types/resource");
import resource = require("resource");

export interface ContentIn {
    payload: ContentPayloadIn;
}

/**
 * A resource that represents a {@link Cell}'s content.
 */
export interface ContentOut extends resource.Resource {
    /**
     * A reference to the cell to which the content belongs to. Only the creator of that cell can
     * modify the content.
     */
    owner: commonRes.ResourceReference;
    /**
     * The creation {@link Date} of the content.
     */
    createdAt: Date;
    /**
     * The {@link Date} when the content was last modified.
     */
    modifiedAt: Date;
    /**
     * The preview for the content. Contrary to {@link ContentOut#payload}, this value
     * is always available so you can safely use it in client UIs.
     */
    preview: ContentPayloadOut;
    /**
     * The content's payload. 
     * If this attribute is not defined, the full payload and the preview do not differ so you
     * can just use the {@link ContentOut#preview}.
     * If {@link ContentPayloadOut#data} on {@link ContentOut#payload} is missing, you can request the full data in a separate call.
     */
    payload?: ContentPayloadOut;
}

/**
 * Defines values for the type of a {@link ContentPayloadOut}.
 */
var _PayloadTypes = {
    /**
     * This payload represents a preview of the full content.
     */
    preview: "preview",
    /**
     * This payload represents the full content.
     */
    full: "full"
};
Object.freeze(_PayloadTypes);
export var PayloadTypes = _PayloadTypes;

export interface ContentPayloadBase {
    /**
     * The mime type of the data.
     */
    mimeType: string;
}

export interface ContentPayloadIn extends ContentPayloadBase {
    data: any;
}

/**
 * Represents the payload data of a content.
 */
export interface ContentPayloadOut extends ContentPayloadBase {
    /**
     * The type of the payload.
     * @see {@link PayloadTypes}
     */
    type: string;
    /**
     * The size of the data.
     */
    size: number;
    /**
     * The actual payload data. This can be missing in which case the ContentPayload acts
     * as a meta data object only.
     */
    data?: any;
}