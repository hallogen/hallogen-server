import commonRes = require("../../common/types/resource");

export interface Resource {
    _type: string;
    ref: commonRes.ResourceReference;
}
