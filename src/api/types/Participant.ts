import resource = require("resource");

export interface UserParticipantIn {
    username: string;
    password: string;
}

export interface ParticipantOut extends resource.Resource {
    name: string;
    participantType: string;
}