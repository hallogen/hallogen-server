import participant = require("./types/participant");
import utils = require("../utils/utils");
import seraph = require("seraph");
import dbModule = require("./db");
import constants = require("../constants");
import t = require("../types");
import _log = require("../utils/log");
var log = _log.getLog("participantDataService");
var db = dbModule.seraph;

var ParticipantNodeLabel = constants.neo4j.participants.ParticipantNodeLabel;

interface ParticipantNeo4j {
    id?: number;
    hid: t.common.resource.ResourceId;
    name: string;
    participantType: string;
}

interface UserNeo4j extends ParticipantNeo4j {
    pwhash: string;
}

export interface UserQuery {
    hid?: string;
    name?: string;
    pwhash?: string;
}

function participantDataFromNeo4jParticipant(neo4jParticipant: ParticipantNeo4j)
: participant.ParticipantData {
    var userData: participant.ParticipantData = {
        ref: new t.common.resource.IdResourceReference(neo4jParticipant.hid),
        name: neo4jParticipant.name,
        participantType: neo4jParticipant.participantType
    };
    return userData;
}

export function getParticipantAsync(participantRef: t.common.resource.ResourceReference) 
: Promise<t.data.participant.ParticipantData> {
    var participantId = participantRef.id;
    return db.findAsync({ hid: participantId }, false, ParticipantNodeLabel)
        .then(function (results) {
            if (!results || results.length === 0) {
                throw new Error("Participant with id' " + participantId + "' could not be found.");
            }
            else {
                if (results.length > 1) {
                    log.warn("Multiple participants found for hid '" + participantId + "'");
                }
    
                return participantDataFromNeo4jParticipant(results[0]);
            }
    });
}

export function findCellCreatorAsync(cellRef: t.common.resource.ResourceReference)
: Promise<t.data.participant.ParticipantData> {
    var cellId = cellRef.id;
    var query =
        "MATCH (creator:" + ParticipantNodeLabel + ")<-[:" + constants.neo4j.cells.CreatedByRelation + "]-" +
        "(:" + constants.neo4j.cells.CellNodeLabel + " { hid:'" + cellId + "' }) \n" +
        "RETURN creator \n" +
        "LIMIT 1";

    return db.queryAsync(query)
        .then(result => {
            if (!result || result.length === 0) {
                throw new Error("Could not find cell creator.");
            }
            else {
                return participantDataFromNeo4jParticipant(result[0]);
            }
        });
}

export function findUser(data: UserQuery): Promise<participant.ParticipantData> {
    return db.findAsync(data, false, constants.neo4j.participants.ParticipantNodeLabel)
        .then(results => {
            if (!results || results.length === 0) {
                throw new Error("User not found.");
            }
            else {
                if (results.length > 1) {
                    log.warn("Multiple users found for query " + JSON.stringify(data));
                }
                return participantDataFromNeo4jParticipant(results[0]);
            }
        });
}

export function addUserAsync(username: string, pwhash: string): Promise<participant.ParticipantData> {
    var neo4jData: UserNeo4j = {
        hid: utils.newId(),
        name: username,
        pwhash: pwhash,
        participantType: t.data.participant.ParticipantTypes.User
    };
    
    return db.saveAsync(neo4jData, constants.neo4j.participants.ParticipantNodeLabel)
        .then(function(storedUser) {
            return participantDataFromNeo4jParticipant(storedUser);
        });
}

/**
 * Creates the "origin" user. This is the first participant in the system.
 * This function should only be called for bootstrapping purposes.
 * @returns {Promise<ParticipantData>}
 */
export function addOriginUser(): Promise<participant.ParticipantData> {
    var data = {
        hid: constants.specialIds.originParticipantId,
        name: "Origin",
        participantType: "Unknown"
    };
    
    return db.saveAsync(data, constants.neo4j.participants.ParticipantNodeLabel)
        .then(function(storedData) {
            var participantData: participant.ParticipantData = {
                ref: new t.common.resource.IdResourceReference(storedData.hid),
                name: storedData.name,
                participantType: storedData.participantType
            };
            return participantData; 
        });
}


export function findUserByCredentials(username: string, pwhash: string): Promise<participant.ParticipantData> {
    return findUser({ name: username, pwhash: pwhash });
}

export function findUserByNameAsync(username: string): Promise<participant.ParticipantData> {
    return findUser({ name: username });
}

export function findParticipantNode(id: string): Promise<any> {
    return db.findAsync({hid: id}, false, constants.neo4j.participants.ParticipantNodeLabel)
        .then(function (results) {
            if (!results || results.length === 0) {
                throw new Error("Participant with id' " + id + "' could not be found.");
            }
            else {
                if (results.length > 1) {
                    log.warn("Multiple participant nodes found for hid '" + id + "'");
                }

                return results[0];
            }
        });
}