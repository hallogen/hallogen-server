import moment = require("moment");
import Promise = require("bluebird");
import _ = require("lodash");

import t = require("../types");
import dbModule = require("./db");
import utils = require("../utils/utils");
import constants = require("../constants");
import errors = require("../api/types/errors");
import _log = require("../utils/log");
var log = _log.getLog("contentDataService");
var neo4j = dbModule.seraph;
var levelup = dbModule.level;

interface ContentNeo4j {
    id?: number;
    hid: t.common.resource.ResourceId;
    createdAt: Date;
    modifiedAt: Date;
    
    preview_type: string;
    preview_mimeType: string;
    preview_size: number;
    preview_data_ref: string;

    payload_type?: string;
    payload_mimeType?: string;
    payload_size?: number;
    payload_data_ref?: string;
}

type ContentData = t.data.content.ContentData;

var ContentNodeLabel = constants.neo4j.content.ContentNodeLabel;
var OwnedByRelation = constants.neo4j.content.OwnedByRelation;

export function addContentAsync(content: ContentData): Promise<any> {
    function findOwnerNodeAsync(ownerHid: string) {
        return neo4j.findAsync({ hid: ownerHid }, false, constants.neo4j.cells.CellNodeLabel)
            .then(results => {
                if (!results || results.length === 0) {
                    throw new Error("Owner cell with id' " + ownerHid + "' could not be found.");
                }
                else {
                    if (results.length > 1) {
                        log.warn("Multiple cells found for hid '" + ownerHid + "'");
                    }

                    return results[0];
                }
            });
    }
    
    var previewRef = utils.newId();
    var payloadRef = utils.newId();
    
    var storedData: ContentNeo4j = {
        hid: content.ref.id,
        createdAt: content.createdAt,
        modifiedAt: content.modifiedAt,
        
        preview_type: content.preview.type,
        preview_mimeType: content.preview.mimeType,
        preview_size: content.preview.size,
        preview_data_ref: previewRef
    };
    
    // Payload is optional.
    if (content.payload) {
        storedData.payload_type = content.payload.type;
        storedData.payload_mimeType = content.payload.mimeType;
        storedData.payload_size = content.payload.size;
        storedData.payload_data_ref = payloadRef;
    }
    
    var ownerNode;
    return findOwnerNodeAsync(content.owner.id)
        .then(_ownerNode => {
            ownerNode = _ownerNode;
            return neo4j.saveAsync(storedData, ContentNodeLabel);
        })
        .then(createdContentNode => neo4j.relateAsync(createdContentNode, OwnedByRelation, ownerNode, {}))
        .then(() => storeDataAsync(previewRef, content.preview.data))
        .then(() => {
            if (content.payload) {
                return storeDataAsync(payloadRef, content.payload.data);
            }
        });
}

export function getContentAsync(ref: t.common.resource.ResourceReference) : Promise<ContentData> {
    function findOwnerCellIdAsync(contentHid: string): Promise<t.common.resource.ResourceReference> {
        var query =
            "MATCH (:" + ContentNodeLabel + " { hid:'" + contentHid + "' }) -[:" + OwnedByRelation + "]->\n" +
            "(owner:" + constants.neo4j.cells.CellNodeLabel + ")\n" +
            "RETURN owner\n" +
            "LIMIT 1";

        return neo4j.queryAsync(query)
            .then(result => {
                if (!result || !result.length) {
                    return null;
                }
                else {
                    return new t.common.resource.IdResourceReference(result[0].hid);
                }
            });
    }
    
    return neo4j.findAsync({ hid: ref.id }, false, ContentNodeLabel)
        .then(results => {
            if (!results || results.length == 0) {
                return null;
            }
            else {
                var neo4jContent: ContentNeo4j = <ContentNeo4j>results[0];
                
                var ownerRef;
                var previewData;
                var payloadData;
                return findOwnerCellIdAsync(neo4jContent.hid)
                    .then(_ownerRef => {
                        if (_ownerRef === null) {
                            throw errors.createFromMsg(errors.InternalError, "Could not find the owner for content " + neo4jContent.hid + ".");
                        }
                        else {
                            ownerRef = _ownerRef; 
                        }
                    })
                    .then(() => getDataAsync(neo4jContent.preview_data_ref))
                    .then(_previewData => {
                        previewData = _previewData;
                    })
                    .then(() => {
                        if (!_.isUndefined(neo4jContent.payload_data_ref)) {
                            return getDataAsync(neo4jContent.payload_data_ref) 
                        }
                        else {
                            return null;
                        }
                    })
                    .then(_payloadData => {
                        payloadData = _payloadData;
                    })
                    .then(() => {
                        var preview = new t.data.content.ContentPayloadData(
                            neo4jContent.preview_type,
                            neo4jContent.preview_mimeType,
                            neo4jContent.preview_size,
                            previewData
                        );
                        
                        var payload = null;
                        if (!_.isUndefined(neo4jContent.payload_data_ref)) {
                            payload = new t.data.content.ContentPayloadData(
                                neo4jContent.payload_type,
                                neo4jContent.payload_mimeType,
                                neo4jContent.payload_size,
                                payloadData
                            );
                        }
                        
                        return new t.data.content.ContentData(
                            new t.common.resource.IdResourceReference(neo4jContent.hid),
                            ownerRef,
                            neo4jContent.createdAt,
                            neo4jContent.modifiedAt,
                            preview,
                            payload
                        );
                    });
            }
        });
}

function storeDataAsync(dataId: string, data: any): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        levelup.put(dataId, data, err => {
            if (err) {
                reject(err);
            }
            else {
                resolve(Promise.resolve(dataId));
            }
        });
    });
}

function getDataAsync(dataId: string): Promise<any> {
    return new Promise<string>((resolve, reject) => {
        levelup.get(dataId, (err, value) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(value);
            }
        });
    });
}
