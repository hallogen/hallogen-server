import t = require("../types");
import utils = require("../utils/utils");
import permissionFac = require("./permissionFactory");
import participantFac = require("./participantFactory");
import contentFac = require("contentFactory");

type CellIn = t.api.cell.CellIn;
type CellOut = t.api.cell.CellOut;
type CellData = t.data.cell.CellData;

/**
 * Creates a cellData and adds all required fields, except for content, which has to be handled separately.
 * @param cellIn The cell template that should be stored.
 * @param creator The creator of the cell, mostly the current user.
 * @returns {CellData} A new cell data.
 */
export function prepareNewCell(cellIn: CellIn, creator: t.data.participant.ParticipantData) : CellData {
    var now = new Date();
    
    var permissions: t.data.permission.PermissionData[];
    if (cellIn.permissions && cellIn.permissions.length > 0) {
        permissions = cellIn.permissions.map(p => permissionFac.createPermission(p));
    }
    else {
        // Always add the public permission by default.
        permissions = [ permissionFac.createPublicPermission() ];
    }
    
    var cellData: CellData = {
        createdAt: now,
        modifiedAt: now,
        creator: creator,
        parent: new t.common.resource.IdResourceReference(cellIn.parent.id),
        permissions: permissions,
        ref: new t.common.resource.IdResourceReference(utils.newId()),
        // Content has to be handled separately, depending on the type of the content (id or payload).
        content: undefined
    };
     
    
    return cellData;
}

/**
 * Prepares a cell for export. Does not fill the content!
 */
export function makeCellOut(cellData: CellData) : CellOut {
    var cellOut: CellOut = {
        _type: "Cell",
        ref: utils.refs.makeFullCellRef(cellData.ref),
        creator: participantFac.makeParticipantOut(cellData.creator),
        parent: utils.refs.makeFullCellRef(cellData.parent),
        permissions: permissionFac.makePermissionOut(cellData.permissions),
        createdAt: cellData.createdAt,
        modifiedAt: cellData.modifiedAt,
        content: undefined,
        score: -1
    };
    return cellOut;
}
