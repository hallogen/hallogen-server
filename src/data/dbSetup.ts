import Promise = require("bluebird");
import db = require("./db");
import participantDataService = require("./participantDataService");
import contentDataService = require("./contentDataService");
import t = require("../types");
import utils = require("../utils/utils");
import constants = require("../constants");
import cellDataSvc = require("./cellDataService");

function saveCell(cell: t.data.cell.CellData, skipParentLink: boolean = false) {
    return cellDataSvc.addCellAsync(cell, skipParentLink);
}

export function setupNeo4j(): Promise<any> {
    checkSetupAllowed();
    var seraph = db.seraph;
    var dropQuery = "MATCH (n) " + 
                    "OPTIONAL MATCH (n)-[r]-() " +
                    "DELETE n,r";
    
    // Start boostrapping the neo4j database.
    return checkSetupAllowed()
        // 1. Clear old database.
        .then(() => seraph.queryRawAsync(dropQuery))
        .then(() => seraph.index.createIfNoneAsync(constants.neo4j.cells.CellNodeLabel, "hid"))
        // 2. Add origin user.
        .then(participantDataService.addOriginUser)
        // 3. Add root cell.
        .then(() => {
            var rootCell: t.data.cell.CellData = {
                ref: new t.common.resource.IdResourceReference(constants.specialIds.rootCellId),
                parent: new t.common.resource.IdResourceReference(constants.specialIds.noId),
                creator: {
                    ref: new t.common.resource.IdResourceReference(constants.specialIds.originParticipantId),
                    name: "Origin",
                    participantType: "Unknown"
                },
                permissions: [],
                createdAt: new Date(),
                modifiedAt: new Date(),
                content: new t.common.resource.IdResourceReference(constants.specialIds.noId)
            };
            return saveCell(rootCell, true);
        })
        // 4. Add participants cell.
        .then(() => {
            var participantsHomeCell: t.data.cell.CellData = {
                ref: new t.common.resource.IdResourceReference(constants.specialIds.participantsHomeCellId),
                parent: new t.common.resource.IdResourceReference(constants.specialIds.rootCellId),
                creator: {
                    ref: new t.common.resource.IdResourceReference(constants.specialIds.originParticipantId),
                    name: "Origin",
                    participantType: "Unknown"
                },
                permissions: [],
                createdAt: new Date(),
                modifiedAt: new Date(),
                content: new t.common.resource.IdResourceReference(constants.specialIds.noId)
            };
            return saveCell(participantsHomeCell);
        });
}

function checkSetupAllowed(): Promise<any> {
    if (!process.env.HALLOGEN_DBSETUP) {
        return Promise.reject("Environment variable HALLOGEN_DBSETUP not set, refusing to recreate databases.");
    }
    else {
        return Promise.resolve();
    }
}

export function setupAll() : Promise<any> {
    return setupNeo4j();
}