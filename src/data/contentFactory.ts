import t = require("../types");
import contentPreviewFac = require("./contentPreviewFactory");
import utils = require("../utils/utils");

type ContentData = t.data.content.ContentData;
type ContentOut = t.api.content.ContentOut;

export function prepareNewContent(contentIn: t.api.content.ContentIn, ownerCellRef: t.common.resource.ResourceReference) 
    : ContentData {
    var now = new Date();

    var payloadIn = contentIn.payload;
    var payload: t.data.content.ContentPayloadData = {
        mimeType: payloadIn.mimeType,
        type: t.api.content.PayloadTypes.full,
        data: payloadIn.data,
        size: sizeOf(payloadIn.data)
    };
    
    var contentData: t.data.content.ContentData = {
        ref: new t.common.resource.IdResourceReference(utils.newId()),
        owner: ownerCellRef,
        createdAt: now,
        modifiedAt: now,
        preview: contentPreviewFac.createPreviewFor(payload),
        // TODO: We don't have preview processing yet. Until then, we only provide a preview, which
        // always equals the full payload to fulfill the "preview is always available" contract.
        payload: null
    };
    
    return contentData;
}

export function makeContentOut(contentData: ContentData)
    : ContentOut {
    var contentOut: ContentOut = {
        _type: "Content",
        ref: utils.refs.makeFullContentRef(contentData),
        owner: utils.refs.makeFullCellRef(contentData.owner),
        createdAt: contentData.createdAt,
        modifiedAt: contentData.modifiedAt,
        preview: contentData.preview,
        payload: contentData.payload
    };
    return contentOut;
}

function sizeOf(data): number {
    if (typeof data === "string") {
        return data.length;
    }
    else if (Array.isArray(data)) {
        return data.length;
    }
    else if (Buffer.isBuffer(data)) {
        return data.length;
    }
    else {
        return -1;
    }
}