import t = require("../types");
import utils = require("../utils/utils");

type ParticipantData = t.data.participant.ParticipantData;

export function makeParticipantOut(participantData: ParticipantData): t.api.participant.ParticipantOut {
    var participantOut: t.api.participant.ParticipantOut = {
        _type: "Participant",
        // A participant is referenced by its home cell externally.
        ref: utils.refs.makeFullCellRef(participantData.ref),
        name: participantData.name,
        participantType: participantData.participantType
    };
    return participantOut;
}