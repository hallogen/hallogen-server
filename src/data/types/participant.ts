import resource = require("./resource");
import commonRes = require("../../common/types/resource");

var _ParticipantTypes = {
    User: "User"
};
Object.freeze(_ParticipantTypes);
export var ParticipantTypes = _ParticipantTypes;

export class ParticipantData extends resource.ResourceData {
    constructor(
        public ref: commonRes.ResourceReference,
        public name: string,
        public participantType: string) {
        super(ref);
    }
}