import commonRes = require("../../common/types/resource");
import resource = require("./resource");

export class ContentData extends resource.ResourceData {
    constructor(
        public ref: commonRes.ResourceReference,
        public owner: commonRes.ResourceReference,
        public createdAt: Date,
        public modifiedAt: Date,
        public preview: ContentPayloadData,
        public payload: ContentPayloadData) {
        super(ref);
    }
}

export class ContentPayloadData {
    constructor(
        public type: string,
        public mimeType: string,
        public size: number,
        public data: any) {
        
    }
}
