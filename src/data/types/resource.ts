import commonRes = require("../../common/types/resource");

export class ResourceData  {
    constructor(public ref: commonRes.ResourceReference) {
    }
}