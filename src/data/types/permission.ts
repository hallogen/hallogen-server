import commonRes = require("../../common/types/resource");

export class PermissionData {    
    constructor(
        public recipient: commonRes.ResourceReference,
        /**
         * @see {@link AccessTypes}
         */
        public accessType: string   
    ) {
    }
}
