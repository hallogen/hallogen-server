import commonRes = require("../../common/types/resource");
import permission = require("./permission")
import resource = require("./resource");
import content = require("./content");
import participant = require("./participant");

export class CellData extends resource.ResourceData {
    constructor(
        public ref: commonRes.ResourceReference,
        public parent: commonRes.ResourceReference,
        public creator: participant.ParticipantData,
        public permissions: permission.PermissionData[],
        public createdAt: Date,
        public modifiedAt: Date,
        public content: commonRes.ResourceReference) {
        super(ref);
    }
}
