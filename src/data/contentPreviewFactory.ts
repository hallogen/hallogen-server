import t = require("../types");

type Payload = t.data.content.ContentPayloadData;

export function createPreviewFor(data: Payload): Payload {
    // TODO: Implement a flexible mechanism for generating previews.
    // We don't have previews yet.
    
    var preview: Payload = {
        type: t.api.content.PayloadTypes.preview,
        mimeType: data.mimeType,
        size: data.size,
        data: data.data
    };
    return preview;
}