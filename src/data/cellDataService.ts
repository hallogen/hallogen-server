import Promise = require("bluebird");
import dbModule = require("./db");
import t = require("../types");
import e = require("../api/types/errors");
import participantDataSvc = require("./participantDataService");
import constants = require("../constants");
import utils = require("../utils/utils");
import _log = require("../utils/log");
var log = _log.getLog("cellDataService");
var db = dbModule.seraph;

type CellData = t.data.cell.CellData;
var CellNodeLabel = constants.neo4j.cells.CellNodeLabel;
var HasChildRelation = constants.neo4j.cells.HasChildRelation;
var CreatedByRelation = constants.neo4j.cells.CreatedByRelation;


interface CellNeo4j {
    id?: number;
    hid: t.common.resource.ResourceId;
    permissions: string;
    createdAt: Date; 
    modifiedAt: Date;
    contentHid: t.common.resource.ResourceId;
}

function cellDataFromNeo4jCell(neo4jCell: CellNeo4j, 
                               parentCellRef: t.common.resource.ResourceReference, 
                               creator: t.data.participant.ParticipantData)
    : t.data.cell.CellData {
    var cellData: t.data.cell.CellData = {
        ref: new t.common.resource.IdResourceReference(neo4jCell.hid),
        parent: parentCellRef,
        creator: creator,
        permissions: JSON.parse(neo4jCell.permissions),
        createdAt: neo4jCell.createdAt,
        modifiedAt: neo4jCell.modifiedAt,
        content: new t.common.resource.IdResourceReference(neo4jCell.contentHid)
    };
    return cellData;
}

export function createHomeCell(participant: t.data.participant.ParticipantData) {
    var now = new Date();
    var homeCell = new t.data.cell.CellData(
        participant.ref, //new t.common.resource.IdResourceReference(utils.newId()),
        new t.common.resource.IdResourceReference(constants.specialIds.participantsHomeCellId),
        participant,
        [ 
            new t.data.permission.PermissionData(
                new t.common.resource.IdResourceReference(constants.specialIds.noId),
                t.common.permission.AccessTypes.append) 
        ],
        now, now,
        new t.common.resource.IdResourceReference(constants.specialIds.noId)
    );
    
    return addCellAsync(homeCell);
}

function findParentCellRefAsync(cellRef: t.common.resource.ResourceReference)
: Promise<t.common.resource.ResourceReference> {
    var cellId = cellRef.id;
    var query =
        "MATCH (parent:" + CellNodeLabel + ")-[:" + HasChildRelation + "]->" +
        "(:" + CellNodeLabel + " { hid:'" + cellId + "' }) \n" +
        "RETURN parent \n" + 
        "LIMIT 1";

    return db.queryAsync(query)
        .then(result => {
            if (!result || result.length === 0) {
                throw new Error("Could not find parent cell.");
            }
            else {
                return new t.common.resource.IdResourceReference(result[0].hid);
            }
        });
}

export function findCreatorAsync(cellRef: t.common.resource.ResourceReference) : Promise<t.common.resource.ResourceReference> {
    var cellId = cellRef.id;
    var query =
        "MATCH (:" + CellNodeLabel + " { hid:'" + cellId + "' }) -[:" + CreatedByRelation + "]->\n" +
        "(creator:" + constants.neo4j.participants.ParticipantNodeLabel + ")\n" +
        "RETURN creator\n" +
        "LIMIT 1";

    return db.queryAsync(query)
        .then(result => {
            if (!result.length) {
                return null;
            }
            else {
                return new t.common.resource.IdResourceReference(result[0].hid); 
            }
        });
}

export function findCellNode(id: string) : Promise<any> {
    return db.findAsync({ hid: id }, false, CellNodeLabel)
        .then(results => {
            if (!results || results.length === 0) {
                throw new Error("Cell with hid' " + id + "' could not be found.");
            }
            else {
                if (results.length > 1) {
                    log.warn("Multiple cells found for hid '" + id + "'");
                }

                return results[0];
            }
        });
}

export function getCellAsync(cellRef: t.common.resource.ResourceReference, resolveParent = true) : Promise<t.data.cell.CellData> {
    return db.findAsync({ hid: cellRef.id }, false, CellNodeLabel)
        .then(function(results) {
            if (!results || results.length == 0) {
                return null;
            }
            else {
                var neo4jCell = results[0];
                var findParentPromise;
                if (resolveParent) {
                    findParentPromise = findParentCellRefAsync(cellRef);
                }
                else {
                    findParentPromise = Promise.resolve(null);
                }
                
                // Resolve parent cell id.
                return findParentPromise
                    .then(parentCellRef => {
                        // Resolve creator.
                        return participantDataSvc.findCellCreatorAsync(cellRef)
                            .then(creator => {
                                // Build CellData object.
                                return cellDataFromNeo4jCell(neo4jCell, parentCellRef, creator);
                            });
                    });
            }
        });
}

export function getCellPermissionsAsync(cellRef: t.common.resource.ResourceReference): Promise<t.data.permission.PermissionData[]> {
    var cellId = cellRef.id;
    var query = 
        "MATCH (cell:" + CellNodeLabel + " { hid:'" + cellId + "' }) \n" +
        "RETURN cell \n" +
        "LIMIT 1";

    return db.queryAsync(query)
        .then(result => {
            if (!result || result.length === 0) {
                throw new Error("Could not find cell with id " + cellId + ".");
            }
            else {
                return JSON.parse(result[0].permissions);
            }
        });
}

export function getHomeCellAsync(participant: t.data.participant.ParticipantData)
    : Promise<t.data.cell.CellData> {
    return getCellAsync(participant.ref);
}

export function addCellAsync(cell: CellData, skipParentLink = false) : Promise<any> {
    var storedData: CellNeo4j = {
        hid: cell.ref.id,
        permissions: JSON.stringify(cell.permissions),
        createdAt: cell.createdAt,
        modifiedAt: cell.modifiedAt,
        contentHid: cell.content.id
    };
    
    var parentCellId = cell.parent && cell.parent.id;
    if (!parentCellId) {
        return Promise.reject("Cell does not have a valid parent cell id.");
    }
    var creatorId = cell.creator && cell.creator.ref && cell.creator.ref.id;
    if (!creatorId) {
        return Promise.reject("Cell does not have a valid creator id.");
    }

    var parentCellNode;
    var creatorNode;
    
    // The function to find the parent might be a noop if we don't link the cell to a parent
    // (this is only the case for the root cell right now).
    var findParent;
    if (!skipParentLink) {
        findParent = () => findCellNode(parentCellId).then(node => parentCellNode = node);
    }
    else {
        findParent = () => Promise.resolve();
    }

    // The function to link the new cell to its parent might be a noop if we don't have a parent
    // (this is only the case for the root cell right now).
    var linkToParentFn;
    if (!skipParentLink) {
        // Link the cell to the parent.
        linkToParentFn = createdCellNode => db.relateAsync(parentCellNode, HasChildRelation, createdCellNode, {});
    }
    else {
        linkToParentFn = () => Promise.resolve();
    }

    // The function to find the creator node so that we can link to it.
    var findCreatorFn = participantDataSvc.findParticipantNode(creatorId).then(participantNode => creatorNode = participantNode);
    // The function that establishes the createdBy relation between our new cell and its creator. 
    var linkToCreatorFn = createdCellNode =>
        db.relateAsync(createdCellNode, CreatedByRelation, creatorNode, {})
            .then(() => createdCellNode);
    
    
    // First obtain all nodes to that we have to link to.
    // Then add the cell node and link it.
    
    // 1. Find the parent node.
    return findParent()
        // 2. Find the creator node.
        .then(findCreatorFn)
        // 3. Save the cell to the graph.
        .then(() => db.saveAsync(storedData, CellNodeLabel))
        // 4. Link the cell to its creator.
        .then(linkToCreatorFn)
        // 5. Link the cell to its parent.
        .then(linkToParentFn);
}

export function deleteCellAsync(cellOrId: CellData | number): Promise<any> {
    var idPromise;
    if (typeof cellOrId === "number") {
        idPromise = Promise.resolve(cellOrId);
    }
    else {
        idPromise = findCellNode(cellOrId.ref.id).then(result => {
            return result.id;
        });
    }
    
    return idPromise.then(id => db.deleteAsync(id, true));
}