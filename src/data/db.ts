import Promise = require("bluebird");
import config = require("./../config");
import _seraph = require("seraph");
var levelup = require("level");

var _level = levelup(config.leveldbDir);
export var level = _level;

var seraphInstance: _seraph.Seraph = <any>Promise.promisifyAll(_seraph(config.neo4jdb));
Promise.promisifyAll(seraphInstance.index);

//export var seraph: any = seraphInstance;
export var seraph: _seraph.Seraph = seraphInstance;