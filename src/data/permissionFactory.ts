import t = require("../types");
import constants = require("../constants");

type PermissionData = t.data.permission.PermissionData;

export function createPublicPermission() : PermissionData {
    var permission: PermissionData = { 
        recipient: new t.common.resource.IdResourceReference(constants.specialIds.noId), 
        accessType: t.common.permission.AccessTypes.append 
    };
    return permission;
}

export function createPermission(permissionIn: t.api.permission.Permission): PermissionData {
    // The types are compatible.
    return permissionIn;
}

export function makePermissionOut(permissionData: PermissionData[]): t.api.permission.Permission[] {
    return permissionData;
}