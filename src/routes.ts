import restify = require("restify");
import authApi = require("./api/authApi");
import cellApi = require("./api/cellApi");
import constants = require("./constants");
import config = require("./config");
import utils = require("./utils/utils");
import t = require("./api/types/types");
import _log = require("./utils/log");
var log = _log.getLog("routes");

export function setup(server: restify.Server) {
    // Cells
    server.post(utils.applyBasePath(constants.resourcePaths.cell), cellApi.createCellRequest);
    server.get(utils.applyBasePath(constants.resourcePaths.cell + "/:cellid"), cellApi.getCellRequest);

    // Authentication
    server.post(utils.applyBasePath(constants.resourcePaths.auth + "/accesstoken"), authApi.createAccessToken);
    server.post(utils.applyBasePath(constants.resourcePaths.auth + "/participants"), authApi.createParticipant);
    server.get(utils.applyBasePath(constants.resourcePaths.auth + "/participants/me"), authApi.whoami);
    
    //server.get(".*", (req, res, next) => {
    //    if (config.debug) {
    //        log.info("Not found: " + req.path());
    //    }
    //    return next(t.errors.createFromMsg(t.errors.NotFoundError, "Not found."));
    //});
}