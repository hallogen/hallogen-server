/// <reference path="../bluebird/bluebird.d.ts" />

declare module "seraph" {
    
    function seraph(server:string):seraph.Seraph;
    function seraph(options:seraph.InitOptions):seraph.Seraph;
    export = seraph;
    
    module seraph {
        
        interface InitOptions {
            server?: string;
            endpoint?: string;
            id?: string;
        }
        
        interface Relationship {
            start: number;
            end: number;
            type: string;
            properties: Object;
            id: number;
        }
        
        interface ErrorCallback {
            (err:Error);
        }

        interface ResultCallback {
            (err:Error, result:Object[]);
        }

        interface NodeResultCallback {
            (err:Error, node:Object);
        }
        
        interface NodeListResultCallback {
            (err:Error, nodes: Object[]);
        }
        
        interface RelationshipResultCallback {
            (err:Error, relationship: Relationship);
        }

        interface ResponseCallback {
            (err:Error, result:any, response:any);
        }
        
        interface IndexCallback {
            (err:Error, index:Index): void;
        }
        
        interface Index {
            label: string;
            property_keys: Array<string>
        }

        interface Operation {

        }
        
        interface IndexFunctions {
            create(label: string, key: string, callback: IndexCallback): void;
            createAsync(label: string, key: string): Promise<Index>;
            createIfNone(label: string, key: string, callback: IndexCallback): void;
            createIfNoneAsync(label: string, key: string): Promise<Index>;
        }

        interface Seraph {
            query(query:string, callback:ResultCallback) : void;
            query(query:string, params:Object, callback:ResultCallback) : void;
            queryAsync(query:string) : Promise<any>;
            queryAsync(query:string, params:Object) : Promise<any>;
            queryRaw(query:string, callback:ResultCallback) : void;
            queryRaw(query:string, params:Object, callback:ResultCallback) : void;
            queryRawAsync(query:string) : Promise<any>;
            queryRawAsync(query:string, params:Object) : Promise<any>;

            operation(path:string) : Operation;
            operation(path:string, data:Object) : Operation;
            operation(path:string, method:string, data:Object) : Operation;

            call(operation:Operation, callback:ResponseCallback) : void;

            /**
             * Create or update a node.
             * If object has an id property, the node with that id is updated.
             * Otherwise, a new node is created. Returns the newly created/updated node to the callback.
             * ** Note: using node.save with a label does not work in a batch.
             * If you want to create a node with label in a batch, you should call node.save without a label,
             * followed by node.label with a reference to the created node. **
             * @param object an object to create or update
             * @param label
             *     a label to label this node with. this is performed atomically, so if labelling the node fails,
             *     the node is not saved/updated. supplying label is exclusive with key and value. You may either
             *     specify a label, or a key and a value, but all three. ** Note: using node.save with a label does not
             *     work in a batch. If you want to create a node with label in a batch, you should call node.save
             *     without a label, followed by node.label with a reference to the created node. **
             * @param callback
             *     node is the newly saved or updated node. If a create was performed, node will now have
             *     an id property. The returned object is not the same reference as the passed object (the passed
             *     object will never be altered)
             */
            save(object:Object, label:string, callback: NodeResultCallback) : void;
            save(object:Object, key:string, value:string, callback: NodeResultCallback) : void;
            saveAsync(object:Object, key:string, value:string) : Promise<any>;
            saveAsync(object:Object, label:string) : Promise<any>;

            relate(object:number|Object, type:string, secondObject:number|Object, callback: RelationshipResultCallback);
            relate(object:number|Object, type:string, secondObject:number|Object, properties: Object, callback: RelationshipResultCallback);
            relateAsync(object:number|Object, type:string, secondObject:number|Object, properties?: Object) : Promise<Relationship>
            
            find(predicate: Object, any: boolean, label: string, callback: NodeListResultCallback);
            findAsync(predicate: Object, any: boolean, label: string) : Promise<any[]>;

            /**
             * Delete a node.
             * @param node either the id of the node to delete, or an object containing an id property of the node to delete.
             * @param forceOrProperty (optional - default = false) - if truthy, will delete all the node's relations prior to deleting the node. 
             *                        if property, delete only the property with this name on the object. 
             *                        note that you can either specify property or force, not both, as force is meaningless when deleting a property
             * @param callback if err is falsy, the node has been deleted.
             */
            delete(node:string|Object, forceOrProperty: boolean | string, callback: ErrorCallback);
            deleteAsync(node:string|Object, forceOrProperty?: boolean | string) : Promise<Error>

            index: IndexFunctions;
        }
    }
}
