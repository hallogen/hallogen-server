interface SinonStub {
    resolves(value: any): SinonStub;
    rejects(error: any): SinonStub;
}

declare module "sinon-as-promised" {
    function init(promise: any);
    export = init;
}