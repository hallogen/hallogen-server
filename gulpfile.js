var path = require("path");
var gulp = require("gulp");
var ts = require("gulp-typescript");
var sourcemaps = require("gulp-sourcemaps");
var mocha = require("gulp-mocha");
var eventStream = require("event-stream");
var devServer = require("gulp-develop-server");
var del = require("del");
var prompt = require("prompt");
var shell = require("shelljs");
var fs = require("fs");

var BUILD_DIR_NAME = "build";
var APP_DIR_NAME = "src";
var TEST_DIR_NAME = "test";
var SCHEMA_DIR_NAME = "schema";

/** Paths setup **/
var appDir = APP_DIR_NAME;
var testDir = TEST_DIR_NAME;
var schemaDir = SCHEMA_DIR_NAME;

//var typingsFile = "typings/tsd.d.ts";
var typingFiles = path.join("typings", "**", "*.d.ts");
var appFiles = path.join(appDir, "**", "*.ts"); 
var testFiles = path.join(testDir, "**", "*.ts");
var appBuiltFiles = [ path.join(appDir, "**", "*.js"), path.join(appDir, "**", "*.js.map") ];
var testBuiltFiles = [ path.join(testDir, "**", "*.js"), path.join(testDir, "**", "*.js.map") ];

/** Typescript build setup **/
var tsOptions = {
    module: "commonjs",
    target: "ES5"
};
var tsProject = ts.createProject(tsOptions);
var tsTestProject = ts.createProject(tsOptions);

/** Helpers **/
/**
 * Generates a configuration object for the sourcemaps task.
 * @param srcDir The directory in which the typescript files are located.
 * @param targetDir The directory in which the compiled js and source map files are located.
 * @returns {{includeContent: boolean, sourceRoot: Function}}
 */
function makeSourceMapsConfig(srcDir, targetDir) {
    return {
        includeContent: false,
        sourceRoot: function(file) {
            // Map the source map files to the correct originating file with a relative path.
            // With includeContent=true, IDEs don't know which file to map the source to.  
            var fileDir = path.join(targetDir, path.dirname(file.sourceMap.file));
            var relPath = path.relative(fileDir, srcDir);
            return relPath;
        }
    };
}

/** Tasks **/
gulp.task("compile", function() {
    var srcStream = gulp.src([
            appFiles
        ])
        .pipe(sourcemaps.init())
        .pipe(ts(tsProject))
        .pipe(sourcemaps.write("."));
        // Point to the original source files so that we can debug on them.
        //.pipe(sourcemaps.write(".", makeSourceMapsConfig(appDir, appBuildDir)));

    var testStream = gulp.src([
            testFiles
        ])
        .pipe(sourcemaps.init())
        .pipe(ts(tsTestProject))
        .pipe(sourcemaps.write("."));
        //.pipe(sourcemaps.write(".", makeSourceMapsConfig("./", testBuildDir)));

    // Merge the two output streams, so this task is finished when the IO of both operations is done.
    return eventStream.merge(
        srcStream.pipe(gulp.dest(appDir)),
        testStream.pipe(gulp.dest(testDir))
    );
});

gulp.task("test", function () {
    process.env.NODE_ENV = "test";
    return gulp.src(testBuiltFiles, { read: false })
               .pipe(mocha({
                }));
});

gulp.task("watch", [ "compile" ], function() {
    gulp.watch([ appFiles, testFiles, typingFiles ], [ "compile" ]);
});

gulp.task("watchtest", function() {
    gulp.watch([ appBuiltFiles, testBuiltFiles, typingFiles ], [ "test" ]);
});

gulp.task("run", function() {
    devServer.listen({
        path: path.join(appDir, "app.js"),
        env: {
            NODE_ENV: "dev"
        }
    });
});

gulp.task("clean", function (cb) {
    del([].concat(appBuiltFiles).concat(testBuiltFiles), cb);
});

gulp.task("dbsetup", function(cb) {
    prompt.start();
    prompt.get({
        properties: {
            confirm: {
                description: "This will drop ALL your databases (SQL and Neo4j)!! Do you want to continue (y/N)",
                default: "n"
            }
        }
    }, function(err, result) {
        if (result.confirm === "y") {
            process.env.HALLOGEN_DBSETUP = true;
            var dbSetup = require("./" + appDir + "/data/dbSetup");
            dbSetup.setupAll()
                .then(function() {
                    cb();
                }, function(err) {
                    console.error(err);
                    cb(err);
                });
        }
        else {
            cb();
        }
    });
});

gulp.task("gen-schema", function() {
    
    function schema() {

        var typson = require("typson");
        var File = require("vinyl");
        
        var pendingCount = 0;
        var hasSourceEnded = false;
        var stream;

        function beginWrite() {
            pendingCount++;
        }
        function endWrite() {
            pendingCount--;
            if (pendingCount == 0 && hasSourceEnded) {
                stream.emit("end");
            }
        }

        return eventStream.through(function write(data) {
            stream = this;
            beginWrite();
            typson.definitions(data.path, "").then(function(schema) {
                if (Object.keys(schema).length === 0) {
                    // Drop schema.
                }
                else {
                    for(var prop in schema) {
                        var file = new File({
                            path: prop + ".json",
                            contents: new Buffer(JSON.stringify(schema[prop], null, 4))
                        });
                        stream.emit("data", file);
                    }
                }
            })
                .finally(endWrite);
        }, function end() {
            hasSourceEnded = true;
            if (pendingCount == 0) {
                this.emit("end");
            }
        });
    }
    
    var typesDir = path.join(appDir, "api", "types");
    
    return gulp.src(path.join(typesDir, "**", "*.ts"), { read: false })
        .pipe(schema())
        .pipe(gulp.dest(schemaDir));
});

gulp.task("n4jtestserver", function() {
    var neo4jStartScript;
    
    if (/^win/.test(process.platform)) {
        var winScriptPath = ".\\tools\\neo4j-server\\bin\\Neo4j.bat";
        if (!fs.existsSync(winScriptPath)) {
            throw new Error("Neo4j executable not found. Please download the neo4j server and put it into tools/neo4j-server.")
        }
        neo4jStartScript = winScriptPath; 
    }
    else {
        throw new Error("Unknown platform. Please extend this task to support your platform.");
    }
    
    shell.exec(neo4jStartScript);
});

gulp.task("default", [ "compile" ]);
/****/