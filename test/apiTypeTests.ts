import assert = require("assert");
import e = require("../src/api/types/errors");
require("source-map-support").install();

describe("apiTypes", () => {
    describe("errors", () => {
        var message = "Something went wrong";
        var userMessage = "It's your fault.";
        var errorCode = 1001;

        it("should populate values correctly using a message", () => {
            var error = e.createFromMsg(e.BadRequestError, message);
            assert.equal(error.message, message);
            assert.equal(error.body.message, message);
            // Bad request always uses statusCode 400.
            assert.equal(error.statusCode, 400);
        });
        it("should populate values correctly using an error", () => {
            var jsError = new Error(message);
            var error = e.createFromError(e.BadRequestError, jsError);

            var expectedMessage = error.message;
            assert.equal(error.message, expectedMessage);
            assert.equal(error.body.message, expectedMessage);
            assert.equal(error.statusCode, 400);
        });
        it("should populate values correctly using a message and an error code", () => {
            var error = e.createFromMsgErrorCode(e.BadRequestError, message, errorCode);

            assert.equal(error.message, message);
            assert.equal(error.body.message, message);
            assert.equal(error.body.code, errorCode);
            assert.equal(error.statusCode, 400);
        });
        it("should populate values correctly using a message, a user message and an error code", () => {
            var error = e.createFromMsgErrorCodeUserMsg(e.BadRequestError, message, userMessage, errorCode);

            assert.equal(error.message, message);
            assert.equal(error.body.message, message);
            assert.equal(error.body.userMessage, userMessage);
            assert.equal(error.body.code, errorCode);
            assert.equal(error.statusCode, 400);
        });
        it("should populate values correctly using a message, a user message, an error code and a link with more info", () => {
            var moreInfoLink = "http://example.com";
            var error = e.create(e.BadMethodError, message, userMessage, moreInfoLink, errorCode);

            assert.equal(error.message, message);
            assert.equal(error.body.message, message);
            assert.equal(error.body.userMessage, userMessage);
            assert.equal(error.body.moreInfoLink, moreInfoLink);
            assert.equal(error.body.code, errorCode);
            assert.equal(error.statusCode, 405);
        });

    });
});