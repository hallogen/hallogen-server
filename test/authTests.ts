import assert = require("assert");
import supertest = require("supertest");
import promise = require("bluebird");
import sinon = require("sinon");
import sinonAsPromised = require("sinon-as-promised");
import participantService = require("../src/data/participantDataService");
import participant = require("../src/data/types/participant");
import server = require("../src/server");
import cryptoTools = require("../src/utils/halCrypto");
import utils = require("../src/utils/utils");
require("source-map-support").install();

sinonAsPromised(promise);

describe("authentication", function() {
    var username = "testuser";
    var pw = "1234";
    var userid = utils.newId();
    var tokenEndpoint = utils.applyBasePath("/auth/accesstoken");
    
    var sandbox;
    beforeEach(function() {
        sandbox = sinon.sandbox.create();
        
        var userObj: participant.ParticipantData = {
            ref: { id: userid },
            name: username,
            participantType: "User"
        };
        
        var findUserStub = sandbox.stub(participantService, "findUserByCredentials");
        findUserStub.withArgs(username, cryptoTools.sha256(pw)).resolves(userObj);
        findUserStub.withArgs(sinon.match.any, sinon.match.any).rejects("User not found");
    });
    
    afterEach(function() {
        sandbox.restore();
    });

    it("should successfully create an access token", function(done) {
        supertest(server).post(tokenEndpoint)
            .send({
                username: username,
                password: pw
            })
            .expect(200)
            .expect((res) => {
                assert.ok(res.body.bearerToken, "No bearer token received.");
            })
            .end(done);
    });

    it("should not create tokens for unknown users", function(done) {
        supertest(server).post(tokenEndpoint)
            .send({
                username: "some other user",
                password: pw
            })
            .expect(401)
            .end(done);
    });
    
    it("should not create a token for a user with an incorrect password", function(done) {
        supertest(server).post(tokenEndpoint)
            .send({
                username: username,
                password: "incorrect password"
            })
            .expect(401)
            .end(done);
    });

    it("should use a bearer token for authentication", function(done) {
        // Use supertest.agent for correct cookie handling (no idea why...)
        var srv = supertest.agent(server);
        var bearerToken;
        srv.post(tokenEndpoint)
            .send({
               username: username,
               password: pw
            })
            .expect(200)
            .expect((res) => {
                bearerToken = res.body.bearerToken;
            }) 
            .end(function() {
                srv.get(utils.applyBasePath("/auth/participants/me"))
                    .set("Authorization", "Bearer " + bearerToken)
                    .expect(res => {
                        assert.equal(res.body.name, username);
                    })
                    .end(done);
            });
    });

    it("should not authenticate with an invalid token", function(done) {
        supertest(server).get(utils.applyBasePath("/auth/participants/me"))
            .set("Authorization", "Bearer jwekrhj2345sdfjkl")
            .expect(401)
            .end(done);
    });
});
