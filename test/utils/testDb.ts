/// <reference path="../../typings/tsd.d.ts" />
import fs = require("fs");
import path = require("path");
import Promise = require("bluebird");
import dbSetup = require("../../src/data/dbSetup");
import db = require("../../src/data/db");
import t = require("../../src/types");
import utils = require("../../src/utils/utils");
import cellDataSvc = require("../../src/data/cellDataService");
import participantSvc = require("../../src/services/participantService");
import authSvc = require("../../src/services/authService");
import _log = require("../../src/utils/log");
var log = _log.getLog("testDb");

function checkTestEnvironment() {
    var nodeenv = process.env.NODE_ENV;
    if (!nodeenv || nodeenv.toLowerCase() !== "test") {
        return Promise.reject("No running in test environment. Refusing to modify database.");
    }
    else {
        process.env.HALLOGEN_DBSETUP = true;
        return Promise.resolve();
    }
}

export function setupNeo4j()
    : Promise<any> {
    return checkTestEnvironment()
        .then(dbSetup.setupNeo4j)
}

export interface AuthenticatedTestUser extends t.data.participant.ParticipantData {
    authToken: string;
}

export function prepareUserAsync(name: string) : Promise<AuthenticatedTestUser> {
    return participantSvc.createUserParticipant(name, "12345")
        .then(function(newParticipant) {
            var result = <AuthenticatedTestUser>newParticipant;
            result.authToken = authSvc.createToken({
                participant: newParticipant
            });
            return result; 
        });
}

export function createCellAsync(cell: t.data.cell.CellData) {
    return cellDataSvc.addCellAsync(cell, false)
        .then(() => cell);
}

//export function importCells(csvFile) {
//    var basePath;
//    if (process.platform == "win32") {
//        basePath = "file:" + process.cwd().replace(/\\/g, "/"); 
//    }
//    else {
//        basePath = "file://" + process.cwd();
//    }
//    var csvFullPath = basePath + "/test/db/" + csvFile;
//    return db.seraph.queryAsync(
//                            "LOAD CSV WITH HEADERS FROM '" + csvFullPath + "' AS row " +
//                            "CREATE (c:Cell { "+
//                            "hid: row.hid, " +
//                            "permissions: row.permissions, " +
//                            "createdAt: row.createdAt, " +
//                            "modifiedAt: row.modifiedAt, " +
//                            "contentHid: row.contentHid " + 
//                            "})");
//}

export function setupAll() 
    : Promise<any> {
    return setupNeo4j();
    
}