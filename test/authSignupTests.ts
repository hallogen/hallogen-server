import assert = require("assert");
import supertest = require("supertest");
//import request = require("superagent");
import promise = require("bluebird");
import sinon = require("sinon");
import sinonAsPromised = require("sinon-as-promised");
import _ = require("lodash");

import participantDataSvc = require("../src/data/participantDataService");
import cellDataSvc = require("../src/data/cellDataService");
import server = require("../src/server");
import cryptoTools = require("../src/utils/halCrypto");
import t = require("../src/types");
import testDb = require("./utils/testDb");
import utils = require("../src/utils/utils");
import constants = require("../src/constants");

require("source-map-support").install();

sinonAsPromised(promise);

describe("authentication", () => {
    
    var participantsEndpoint = utils.applyBasePath("/auth/participants");
    
    var sandbox;
    beforeEach(function() {
        sandbox = sinon.sandbox.create();
    });

    afterEach(function() {
        sandbox.restore();
    });

    it("should create a new user", done => {
        var username = "testuser";
        var pw = "12345";

        testDb.setupAll()
            .then(() => {
                supertest(server).post(participantsEndpoint)
                    .send({
                        username: username,
                        password: pw
                    })
                    .expect(201)
                    .expect(res => {
                        assert.equal(res.body.name,  username);
                    })
                    .end(done);
            });
    });

    it("should not be able to create a user that already exist", done => {
        var username = "testuser";
        var pw = "12345";

        var findUserStub = sandbox.stub(participantDataSvc, "findUserByNameAsync");
        findUserStub.withArgs(username).resolves({
            ref: { id: 1 },
            name: username,
            participantType: "User"
        });

        supertest(server).post(participantsEndpoint)
            .send({
                username: username,
                password: pw
            })
            .expect(400)
            .end(done);
    });

    it("should not create too long usernames", done => {
        var looooongUsername = "";
        for (var i = 0; i < 1000; i++) {
            looooongUsername += "a";
        }
        supertest(server).post(participantsEndpoint)
            .send({
                username: looooongUsername,
                password: "12345"
            })
            .expect(400)
            .end(done);
    });

    it("should not create username with unusual characters", function(done) {
        var usersToTest = [
            "?", "§", ".", ",", "+"
        ];

        this.timeout(10000);

        var doneCount = 0;
        for (var i = 0; i < usersToTest.length; i++) {
            var username = usersToTest[i];

            supertest(server).post(participantsEndpoint)
                .send({
                    username: username,
                    password: "12345"
                })
                .expect(400)
                .end(function() {
                    doneCount++;
                    if (doneCount === usersToTest.length) {
                        done();
                    }
                });
        }
    });

    it("should not create usernames with invalid passwords", function(done) {
        var passwordsToTest = [
            ""
        ];

        this.timeout(10000);

        var doneCount = 0;
        for (var i = 0; i < passwordsToTest.length; i++) {
            var pw = passwordsToTest[i];

            supertest(server).post(participantsEndpoint)
                .send({
                    username: "testuser",
                    password: pw
                })
                .expect(400)
                .end(function() {
                    doneCount++;
                    if (doneCount === passwordsToTest.length) {
                        done();
                    }
                });
        }
    });
    
    describe("home cell creation", () => {
        var createdParticipant: t.api.participant.ParticipantOut;
        var createdHomeCell: t.api.cell.CellOut;
        
        before(done => {
            var srv = supertest(server);
            var username = "testuser";
            var pw = "12345";
            testDb.setupAll()
                .then(() => new Promise((resolve, reject) => {
                    // Create user.
                    srv.post(participantsEndpoint)
                        .send({
                            username: username,
                            password: pw
                        })
                        .expect(201)
                        .expect(res => {
                            createdParticipant = res.body;
                        })
                        .end(err => {
                            if (err) {
                                done(err);
                            }
                            else {
                                resolve(null);
                            }
                        });
                }))
                .then(() => new Promise((resolve, reject) => {
                    // "Login"
                    srv.post(utils.applyBasePath("/auth/accesstoken"))
                        .send({
                            username: username,
                            password: pw
                        })
                        .expect(200)
                        .end((err, res) => {
                            if (err) {
                                done(err);
                            }
                            else {
                                resolve(res.body.bearerToken);
                            }
                        });
                }))
                .then((token) => new Promise(resolve => {
                    // Get home cell for new participant.
                    srv.get(utils.makeRelativePath(createdParticipant.ref.href))
                        .set("Authorization", "Bearer " + token)
                        .expect(200)
                        .end((err, res) => {
                            createdHomeCell = res.body;
                            if (err) {
                                done(err);
                            }
                            else {
                                resolve(null);
                                done();
                            }
                        });
                }));
        });
        
        it("should create a home cell for a new user", () => {
            assert.ok(createdHomeCell);
            assert.equal(createdHomeCell.ref.id, createdParticipant.ref.id);
        });
        
        it("should have made the home cell public", () => {
            assert.ok(_.any(createdHomeCell.permissions, 
                (p: t.api.permission.Permission) => p.recipient.id === constants.specialIds.noId && p.accessType === t.common.permission.AccessTypes.append));
        });
        
        it("should have set the home cell as a parent of the home cell root", () => {
            assert.equal(createdHomeCell.parent.id, constants.specialIds.participantsHomeCellId);
        });
        
        //it("should have given the home cell an empty content", () => {
        //    assert.ok(createdHomeCell.content);
        //});
    });
});