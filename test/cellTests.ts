/// <reference path="../typings/tsd.d.ts" />
import assert = require("assert");
import supertest = require("supertest");
import promise = require("bluebird");
import sinon = require("sinon");
import sinonAsPromised = require("sinon-as-promised");
import moment = require("moment");
import _ = require("lodash");

import t = require("../src/types");
import utils = require("../src/utils/utils");
import server = require("../src/server");
import testDb = require("./utils/testDb");
import contentDataSvc = require("../src/data/contentDataService");
import cellDataSvc = require("../src/data/cellDataService");
import cellFactory = require("../src/data/cellFactory");
import constants = require("../src/constants");
import permissionFac = require("../src/data/permissionFactory");
import dbModule = require("../src/data/db");
var db = dbModule.seraph;
require("source-map-support").install();


describe("cells api", () => {
    
    describe("creating public cells", () => {
        
        // The user is created asynchronously below.
        var user: testDb.AuthenticatedTestUser;
        var startDate = moment();
        var toCreate: t.api.cell.CellIn;
        var createdCell: t.api.cell.CellOut;
        
        before((done) => {
            testDb.setupAll()
                .then(() => testDb.prepareUserAsync("testuser"))
                .then(testuser => {
                    user = testuser;
                    toCreate = {
                        parent: user.ref,
                        permissions: [],
                        content: {
                            payload: {
                                mimeType: "text/plain",
                                data: "A random test message"
                            }
                        }
                    };
                })
                .then(() => {
                    supertest(server).post(utils.applyBasePath("/cells"))
                        .set("Authorization", "Bearer " + user.authToken)
                        .send(toCreate)
                        .expect(201)
                        .expect(res => {
                            createdCell = res.body;
                        })
                        .end(done);
                });
        });
        
        it("should create a cell on POST", () => {
            assert.ok(createdCell);
            assert.ok(createdCell.ref.id);
        });

        it("should add an href to created cells", () => {
            assert.ok(createdCell.ref.href);
        });

        it("should have added the content", () => {
            assert.equal(createdCell.content.preview.data, (<t.api.content.ContentIn>toCreate.content).payload.data);
        });
        
        it("should have added creation/modified dates to the cell", () => {
            var createdAt = moment(createdCell.createdAt);
            var modifiedAt = moment(createdCell.modifiedAt);
            
            assert.ok(createdAt.isSame(modifiedAt));
            // Cell creation shouldn't have taken too long. At least we should catch time zone errors with this check.
            assert.ok(createdAt.isSame(startDate, "hour"));
        });

        it("should have added creation/modified dates to the content", () => {
            var createdAt = moment(createdCell.content.createdAt);
            var modifiedAt = moment(createdCell.content.modifiedAt);

            assert.ok(createdAt.isSame(modifiedAt));
            assert.ok(createdAt.isSame(startDate, "hour"));
        });
        
        it("should have set the creator of the new cell", () => {
            assert.equal(createdCell.creator.ref.id, user.ref.id);
            assert.equal(createdCell.creator.name, user.name);
        });
        
        it("should have set the parent cell correctly", () => {
            assert.equal(createdCell.parent.id, toCreate.parent.id);
        });
        
        it("should have added the public permission", () => {
            assert.equal(createdCell.permissions.length, 1);
            assert.ok(_.any(createdCell.permissions, p => p.recipient.id === constants.specialIds.noId && p.accessType === t.common.permission.AccessTypes.append));
        });
    });
    
    describe("cell creation error handling", () => {
        
        it("should rollback cell creations", (done) =>  {
            var sandbox = sinon.sandbox.create();
            var toCreate: t.api.cell.CellIn;
            var user: testDb.AuthenticatedTestUser;

            testDb.setupAll()
                .then(() => testDb.prepareUserAsync("testuser"))
                .then(testuser => {
                    user = testuser;
                    toCreate = {
                        parent: testuser.ref,
                        permissions: [],
                        content: {
                            payload: {
                                mimeType: "text/plain",
                                data: "A random test message"
                            }
                        }
                    };
                })
                .then(() => {
                    // Creating a relation for the cell should fail. 
                    // At this point, the cell is already created and has to be rolled back.
                    var relateStub = sandbox.stub(db, "relateAsync");
                    relateStub.rejects("Relation failed.");
                    // We cannot spy the cellDataSvc here because it doesn't get the spied proxies internally for some reason.
                    var deleteSpy = sandbox.spy(db, "deleteAsync");
                    var saveSpy = sandbox.spy(db, "saveAsync");

                    supertest(server).post(utils.applyBasePath("/cells"))
                        .set("Authorization", "Bearer " + user.authToken)
                        .send(toCreate)
                        .expect(500)
                        .end(function() {
                            sandbox.restore();
                            saveSpy.firstCall.returnValue.then(createdCellNode => {
                                sinon.assert.calledWith(deleteSpy, createdCellNode.id);
                                done();
                            });
                        });
                });
        });
        
        it("should rollback created cells when content could not be created", (done) => {

            var sandbox = sinon.sandbox.create();
            var toCreate: t.api.cell.CellIn;
            var user: testDb.AuthenticatedTestUser;
            
            testDb.setupAll()
                .then(() => testDb.prepareUserAsync("testuser"))
                .then(testuser => {
                    user = testuser;
                    toCreate = {
                        parent: testuser.ref,
                        permissions: [],
                        content: {
                            payload: {
                                mimeType: "text/plain",
                                data: "A random test message"
                            }
                        }
                    };
                })
                .then(() => {
                    var addContentStub = sandbox.stub(contentDataSvc, "addContentAsync");
                    addContentStub.rejects("Content could not be created.");
                    var deleteSpy = sandbox.spy(db, "deleteAsync");
                    var saveSpy = sandbox.spy(db, "saveAsync");
                    
                    supertest(server).post(utils.applyBasePath("/cells"))
                        .set("Authorization", "Bearer " + user.authToken)
                        .send(toCreate)
                        .expect(500)
                        .end(function() {
                            saveSpy.firstCall.returnValue.then(createdCellNode => {
                                sinon.assert.calledWith(deleteSpy, createdCellNode.id);
                                done();
                            });
                        });
                });
        })
    });
    
    describe("cell permissions", () => {
        var user: testDb.AuthenticatedTestUser;
        var otherUser: testDb.AuthenticatedTestUser;
        
        function prepareParentCell(permissions: t.api.permission.Permission[], creator: t.data.participant.ParticipantData) {
            var cell: t.data.cell.CellData = {
                ref: new t.common.resource.IdResourceReference(utils.newId()),
                parent: otherUser.ref,
                creator: creator,
                permissions: permissions,
                createdAt: new Date(),
                modifiedAt: new Date(),
                content: new t.common.resource.IdResourceReference(constants.specialIds.noId)
            };
            return testDb.createCellAsync(cell);
        }
        
        function getCell(cellRef: t.common.resource.ResourceReference): Promise<supertest.Response> {
            return new Promise<supertest.Response>((resolve, reject) => {
                supertest(server).get(utils.applyBasePath("/cells/" + cellRef.id))
                    .set("Authorization", "Bearer " + user.authToken)
                    .end((err, res) => {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(Promise.resolve(res));
                        }
                    });
            });
        }
        
        function postCell(parentCellRef: t.common.resource.ResourceReference, 
                          preparedCell: t.api.cell.CellIn = null): Promise<supertest.Response> {
            
            var cell: t.api.cell.CellIn = preparedCell || {
                parent: parentCellRef,
                permissions: [],
                content: {
                    payload: {
                        mimeType: "text/plain",
                        data: "A random test message"
                    }
                }
            };
            
            return new Promise<supertest.Response>((resolve, reject) => {
                supertest(server).post(utils.applyBasePath("/cells"))
                    .set("Authorization", "Bearer " + user.authToken)
                    .send(cell)
                    .end((err, res) => {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(Promise.resolve(res));
                        }
                    });
            });
        }

        beforeEach(() => {
            return testDb.setupAll()
                .then(() => testDb.prepareUserAsync("testuser"))
                .then(testuser => {
                    user = testuser;
                })
                .then(() => testDb.prepareUserAsync("otherUser"))
                .then(othertestuser => {
                    otherUser = othertestuser;
                });
        });
        
        var AccessTypes = t.common.permission.AccessTypes;
        var IdRef = t.common.resource.IdResourceReference;
        var PermissionData = t.data.permission.PermissionData;
        
        it("should not allow appending to cells with empty permissions", () => {
            return prepareParentCell([], otherUser)
                .then(parentCell => postCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 403);
                });
        });
        
        it("should allow creators to post to their cells with empty permissions", () => {
            return prepareParentCell([], user)
                .then(parentCell => postCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 201);
                });
        });

        it("should not allow appending if the participant doesn't have permission", () => {
            return prepareParentCell([
                    new PermissionData(new IdRef(utils.newId()), AccessTypes.append)
                ], otherUser)
                .then(parentCell => postCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 403);
                });
        });
        
        it("should not allow appending if the participant only has read access", () => {
            return prepareParentCell([
                new PermissionData(user.ref, AccessTypes.read)
            ], otherUser)
                .then(parentCell => postCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 403);
                });
        });
        
        it("should allow appending when a cell has public append access", () => {
            return prepareParentCell([
                permissionFac.createPublicPermission()
            ], otherUser)
                .then(parentCell => postCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 201);
                });
        });
        
        
        
        it("should not allow reading cells with empty permissions", () => {
            return prepareParentCell([], otherUser)
                .then(parentCell => getCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 403);
                });
        });
        
        it("should allow creators to read their cells with empty permissions", () => {
            return prepareParentCell([], user)
                .then(parentCell => getCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 200);
                });
        });
        
        it("should not allow reading when the participant doesn't have permission", () => {
            return prepareParentCell([
                    new PermissionData(new IdRef(utils.newId()), AccessTypes.read)
                ], otherUser)
                .then(parentCell => getCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 403);
                });
        });
        
        it("should allow reading when the participant has append permission", () => {
            return prepareParentCell([
                new PermissionData(user.ref, AccessTypes.append)
            ], otherUser)
                .then(parentCell => getCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 200);
                });
        });
        
        it("should allow reading when the participant has read permission", () => {
            return prepareParentCell([
                new PermissionData(user.ref, AccessTypes.read)
            ], otherUser)
                .then(parentCell => getCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 200);
                });
        });
        
        it ("should allow reading when the cell has public permission", () => {
            return prepareParentCell([
                permissionFac.createPublicPermission()
            ], otherUser)
                .then(parentCell => getCell(parentCell.ref))
                .then(res => {
                    assert.equal(res.status, 200);
                });
        });

        it("should not allow creating cells with invalid format", () => {
            return prepareParentCell([], user)
                .then(parentCell => {
                    var invalidCell = {
                        parent: parentCell.ref
                        // permissions and content are missing
                    };
                    return postCell(parentCell.ref, <t.api.cell.CellIn>invalidCell);
                })
                .then(res => {
                    assert.equal(res.status, 400);
                });
        });
    });

    describe("retrieving cells", () => {
        it("should fail when trying to get cells with invalid ids", (done) => {
            var srv = supertest(server);
            testDb.setupAll()
                .then(() => testDb.prepareUserAsync("testuser"))
                .then((user) => {
                    srv.get(utils.applyBasePath("/cells") + "/iaminvalid234")
                        .set("Authorization", "Bearer " + user.authToken)
                        .expect(400)
                        .end(done);
                });
        });
        
        it("should fail when trying to get a non-existing cell", (done) => {
            var srv = supertest(server);
            testDb.setupAll()
                .then(() => testDb.prepareUserAsync("testuser"))
                .then((user) => {
                    srv.get(utils.applyBasePath("/cells") + "/" + utils.newId())
                        .set("Authorization", "Bearer " + user.authToken)
                        .expect(404)
                        .end(done);
                });
        });
    });
});