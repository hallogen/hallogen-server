import assert = require("assert");
import sinon = require("sinon");
import path = require("path");
import fs = require("fs");
import tmp = require("tmp");
import errors = require("../src/api/types/errors");
import config = require("../src/config");
import validation = require("../src/services/jsonSchemaValidator");
require("source-map-support").install();
tmp.setGracefulCleanup();

describe.only("json validation", () => {
    
    it("should load schema files from the configured directory", (done) => {
        var originalSchemaPath = config.schemaPath;
        tmp.dir({unsafeCleanup: true}, (err, dir, cleanupCallback) => {
            if (err) throw err;

            config.schemaPath = dir;
            var testSchema = {
                "$schema": "http://json-schema.org/draft-04/schema",
                "definitions": {
                    "TestType": {
                        "type": "object",
                        "properties": {"property1": {"type": "string"}},
                        "required": ["property1"]
                    }
                }
            };
            fs.writeFileSync(path.join(dir, "testschema.json"), JSON.stringify(testSchema));
            
            var succeedingObj = {
                "property1": "value"
            };
            var failingObj = {
                "otherproperty": "value"
            };
            
            validation.validate(succeedingObj, "testschema", "TestType")
                .then(() => {
                    return validation.validate(failingObj, "testschema", "TestType")
                        .then(() => { assert.fail("validation succeeded", "validation failed"); },
                              () => { });
                })
                .finally(() => {
                    config.schemaPath = originalSchemaPath;
                    cleanupCallback();
                })
                .then(done, function(errors) {
                    done(new Error(JSON.stringify(errors)));
                });

        });
    });
    
    it("should allow only valid ids", () => {
        var obj = {
            id: "df8a34986b80448aa501052b0457a79e"
        };
        return validation.validate(obj, "resource", "ResourceReference");
    });
});