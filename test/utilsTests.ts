import assert = require("assert");
import utils = require("../src/utils/utils");
import constants = require("../src/constants");
require("source-map-support").install();

describe("utils", () => {
    describe("refs helper", () => {
        it("should generate a cell resource path correctly", () => {
            var cellId = utils.newId();
            var expectedPath = utils.getApiBasePath() + constants.resourcePaths.cell + "/" + cellId;
            var cellRef = utils.refs.makeFullCellRef(<any>{
                id: cellId
            });
            assert.equal(expectedPath, cellRef.href);
        });
        it("should extract the id from a top level resource ref", () => {
            var id = utils.newId();
            var ref = "https://hallogen.test/cells/" + id;
            assert.equal(utils.refs.extractId(ref), id);
        });
        it("should extract the id from a nested resource ref", () => {
            var id = utils.newId();
            var ref = "https://hallogen.test/cells/" + utils.newId() + "/" + id;
            assert.equal(utils.refs.extractId(ref), id);
        });
    })
});